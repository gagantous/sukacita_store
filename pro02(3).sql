-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2019 at 11:40 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pro02`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `user` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `email` varchar(150) NOT NULL,
  `nohp` varchar(36) NOT NULL,
  `alamat` text NOT NULL,
  `id_role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `nama`, `user`, `password`, `email`, `nohp`, `alamat`, `id_role`) VALUES
(24, 'Admin', 'edric', 'c9913948c5197c26bdd9e8c15088f6fc', '', '090190190191', '', 3),
(30, 'Kasir 1', 'kasir', 'c7911af3adbd12a035b289556d96470a', '', '', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `kode_barang` varchar(20) NOT NULL,
  `nama_barang` varchar(45) NOT NULL,
  `satuan` varchar(45) NOT NULL,
  `harga` int(15) NOT NULL,
  `stok` int(5) NOT NULL,
  `tgl_masuk` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tgl_barang_diupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_admin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `kode_barang`, `nama_barang`, `satuan`, `harga`, `stok`, `tgl_masuk`, `tgl_barang_diupdate`, `id_admin`) VALUES
(1, '', 'Contoh', 'ahahd', 1000000, 22, '2019-01-11 06:25:51', '2019-01-11 06:26:19', 24);

-- --------------------------------------------------------

--
-- Table structure for table `conf_app`
--

CREATE TABLE `conf_app` (
  `id` int(11) NOT NULL,
  `nama_aplikasi` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conf_app`
--

INSERT INTO `conf_app` (`id`, `nama_aplikasi`) VALUES
(1, 'sukacita');

-- --------------------------------------------------------

--
-- Table structure for table `conf_printer`
--

CREATE TABLE `conf_printer` (
  `id` int(11) NOT NULL,
  `nama_toko` varchar(40) NOT NULL,
  `alamat_toko` varchar(40) NOT NULL,
  `bool_alamat` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conf_printer`
--

INSERT INTO `conf_printer` (`id`, `nama_toko`, `alamat_toko`, `bool_alamat`) VALUES
(1, 'sukacita', 'Jln. Penganyoman', 1);

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksi`
--

CREATE TABLE `detail_transaksi` (
  `id` int(11) NOT NULL,
  `id_nota` varchar(20) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `subtotal` int(20) NOT NULL,
  `barang_m` text NOT NULL,
  `harga_m` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_transaksi`
--

INSERT INTO `detail_transaksi` (`id`, `id_nota`, `id_barang`, `qty`, `subtotal`, `barang_m`, `harga_m`) VALUES
(101, '37', 1, 2, 61000, '0', 0),
(102, '39', 1, 4, 122000, '0', 0),
(103, '40', 1, 8, 244000, '0', 0),
(104, '46', 1, 5, 152500, '0', 0),
(105, '47', 1, 1, 30500, '0', 0),
(106, '47', 2, 2, 480000, '0', 0),
(107, '48', 3, 1, 235000, '0', 0),
(108, '48', 0, 6, 600000, '0', 0),
(109, '49', 0, 3, 300000, 'ajhajsh', 0),
(110, '50', 0, 4, 3600000, 'jhkjhkj', 0),
(111, '52', 0, 1, 10000, '90', 10000),
(112, '52', 0, 3, 270000, 'nmbnm', 90000),
(113, '53', 0, 2, 180000, 'Panci', 90000),
(114, '53', 2, 1, 240000, '', 0),
(115, '53', 3, 1, 235000, '', 0),
(116, '54', 1, 2, 61000, '', 0),
(117, '57', 2, 1, 240000, '', 0),
(118, '60', 0, 4, 200000, 'fajhfjah', 50000),
(119, '61', 1, 3, 91500, '', 0),
(120, '62', 1, 3, 150000, '', 0),
(121, '64', 1, 2, 61000, '', 0),
(122, '65', 1, 4, 122000, '', 0),
(123, '66', 0, 1, 20000, 'pel', 20000),
(124, '67', 1, 3, 91500, '', 0),
(125, '67', 0, 2, 20000, 'kain', 10000),
(126, '67', 14, 3, 1455000, '', 0),
(127, '67', 4, 1, 197000, '', 0),
(128, '68', 1, 2, 61000, '', 0),
(129, '69', 17, 1, 485000, '', 0),
(130, '70', 14, 1, 485000, '', 0),
(131, '71', 94, 2, 50000, '', 0),
(132, '72', 13, 1, 315000, '', 0),
(133, '72', 34, 1, 285000, '', 0),
(134, '73', 2, 1, 240000, '', 0),
(135, '73', 7, 1, 270000, '', 0),
(136, '73', 17, 1, 485000, '', 0),
(137, '75', 12, 1, 270000, '', 0),
(138, '76', 37, 2, 730000, '', 0),
(139, '77', 23, 3, 555000, '', 0),
(140, '78', 42, 2, 370000, '', 0),
(141, '79', 1, 1, 30500, '', 0),
(142, '80', 8, 1, 140000, '', 0),
(143, '81', 133, 1, 135000, '', 0),
(144, '82', 13, 1, 315000, '', 0),
(145, '82', 37, 1, 365000, '', 0),
(146, '82', 149, 1, 17000, '', 0),
(147, '82', 42, 1, 185000, '', 0),
(148, '82', 123, 1, 127000, '', 0),
(149, '82', 100, 1, 80000, '', 0),
(150, '83', 1, 2, 61000, '', 0),
(151, '83', 3, 1, 235000, '', 0),
(152, '83', 13, 1, 315000, '', 0),
(153, '83', 34, 1, 285000, '', 0),
(154, '83', 42, 1, 185000, '', 0),
(155, '83', 100, 2, 160000, '', 0),
(156, '84', 172, 2, 40000, '', 0),
(157, '85', 172, 1, 20000, '', 0),
(158, '86', 172, 1, 20000, '', 0),
(159, '88', 107, 2, 178000, '', 0),
(160, '88', 172, 1, 20000, '', 0),
(161, '88', 4, 1, 197000, '', 0),
(162, '89', 5, 1, 215000, '', 0),
(163, '89', 7, 1, 270000, '', 0),
(164, '90', 5, 1, 215000, '', 0),
(165, '91', 2, 1, 240000, '', 0),
(166, '92', 6, 4, 960000, '', 0),
(167, '93', 2, 1, 240000, '', 0),
(168, '94', 4, 10, 1970000, '', 0),
(169, '94', 7, 1, 270000, '', 0),
(170, '96', 8, 3, 420000, '', 0),
(171, '97', 4, 1, 197000, '', 0),
(172, '98', 4, 1, 197000, '', 0),
(173, '99', 8, 1, 140000, '', 0),
(174, '100', 21, 2, 450000, '', 0),
(175, '101', 13, 3, 945000, '', 0),
(176, '102', 12, 2, 540000, '', 0),
(177, '1', 15, 2, 970000, '', 0),
(178, '1', 12, 1, 270000, '', 0),
(179, '2', 14, 2, 970000, '', 0),
(180, '1', 173, 2, 200000, '', 0),
(181, '1', 1, 1, 1000000, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `nota`
--

CREATE TABLE `nota` (
  `id` int(11) NOT NULL,
  `nota` varchar(250) NOT NULL,
  `nama_barang` varchar(1000) NOT NULL,
  `harga_barang` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `bayar` int(11) NOT NULL,
  `terbilang` text NOT NULL,
  `tgl` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `nama_role` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `nama_role`) VALUES
(1, 'admin'),
(2, 'kasir'),
(3, 'super');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `sejumlah` int(11) NOT NULL,
  `tanggal_diubah` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `id_barang`, `sejumlah`, `tanggal_diubah`, `status`) VALUES
(0, NULL, 0, '2017-04-07 16:00:00', ''),
(176, 1, 3, '2017-04-14 00:40:46', 'transaksi'),
(177, 1, 2, '2017-04-13 04:55:05', 'transaksi'),
(178, 1, 4, '2017-04-13 06:03:20', 'transaksi'),
(179, 1, 3, '2017-04-13 06:07:07', 'transaksi'),
(181, 4, 1, '2017-04-13 06:07:07', 'transaksi'),
(182, 4, 1, '2017-04-13 06:09:55', 'Perubahan Informasi data barang'),
(183, 4, 20, '2017-04-13 06:10:03', 'Perubahan Informasi data barang'),
(184, 1, 2, '2017-04-14 06:47:03', 'transaksi'),
(188, 13, 1, '2017-04-15 08:36:31', 'transaksi'),
(190, 2, 1, '2017-04-15 08:37:43', 'transaksi'),
(191, 7, 1, '2017-04-15 08:37:44', 'transaksi'),
(193, 12, 1, '2017-04-16 04:20:45', 'transaksi'),
(197, 1, 1, '2017-04-16 04:23:02', 'transaksi'),
(198, 8, 1, '2017-04-16 04:23:19', 'transaksi'),
(200, 13, 1, '2017-04-16 04:25:38', 'transaksi'),
(206, 1, 2, '2017-04-16 05:23:13', 'transaksi'),
(207, 3, 1, '2017-04-16 05:23:13', 'transaksi'),
(208, 13, 1, '2017-04-16 05:23:13', 'transaksi'),
(218, 3, 2, '2017-04-17 09:04:59', 'Barang dihapus'),
(219, 1, 12, '2017-04-17 09:24:55', 'Barang dihapus'),
(222, 4, 1, '2017-04-17 09:45:27', 'transaksi'),
(223, 5, 1, '2017-04-17 09:48:11', 'transaksi'),
(224, 7, 1, '2017-04-17 09:48:11', 'transaksi'),
(225, 5, 1, '2017-04-21 03:49:18', 'transaksi'),
(226, 2, 1, '2017-04-21 03:52:48', 'transaksi'),
(227, 6, 4, '2017-04-22 03:53:51', 'transaksi'),
(228, 2, 1, '2017-04-22 06:29:51', 'transaksi'),
(229, 4, 10, '2017-07-02 23:20:30', 'transaksi'),
(230, 7, 1, '2017-07-02 23:20:30', 'transaksi'),
(231, 4, 4, '2017-08-25 07:50:04', 'Pengurangan jumlah barang dari stok'),
(232, 8, 3, '2017-10-09 03:59:57', 'transaksi'),
(233, 4, 1, '2017-10-20 07:42:05', 'transaksi'),
(234, 4, 1, '2017-10-20 07:43:33', 'transaksi'),
(235, 8, 1, '2017-10-20 07:43:48', 'transaksi'),
(237, 13, 3, '2018-04-21 01:58:41', 'transaksi'),
(239, 13, -6, '2018-09-27 23:43:01', 'Barang dihapus'),
(241, 16, 0, '2018-09-28 17:06:45', 'Barang dihapus'),
(243, 12, 1, '2018-10-29 04:17:20', 'transaksi'),
(244, 171, 0, '2018-11-11 00:33:12', 'Barang dihapus'),
(245, 170, 0, '2018-11-11 00:33:19', 'Barang dihapus'),
(247, 175, 20, '2018-12-29 04:28:55', 'Barang baru'),
(248, 12, -4, '2018-12-29 04:29:32', 'Barang dihapus'),
(249, 174, 40, '2018-12-29 04:29:39', 'Barang dihapus'),
(250, 175, 20, '2018-12-29 04:29:44', 'Barang dihapus'),
(251, 173, 200, '2018-12-31 03:25:01', 'Barang baru'),
(252, 174, 200, '2018-12-31 04:05:54', 'Barang baru'),
(253, 174, 0, '2018-12-31 04:15:43', 'Perubahan Informasi data barang'),
(254, 173, 180, '2018-12-31 04:33:09', 'Pengurangan jumlah barang dari stok'),
(255, 175, 154, '2018-12-31 04:34:36', 'Barang baru'),
(256, 174, 200, '2018-12-31 04:40:36', 'Barang dihapus'),
(257, 175, 154, '2018-12-31 04:42:04', 'Barang dihapus'),
(258, 173, 0, '2018-12-31 04:42:36', 'Perubahan Informasi data barang'),
(259, 176, 0, '2018-12-31 04:44:44', 'Barang baru'),
(260, 176, 0, '2018-12-31 04:44:57', 'Perubahan Informasi data barang'),
(261, 173, 2, '2018-12-31 05:18:00', 'transaksi'),
(262, 1, 23, '2019-01-11 06:25:51', 'Barang baru'),
(263, 1, 1, '2019-01-11 06:26:19', 'transaksi');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL,
  `no_nota` varchar(250) NOT NULL,
  `tgl_transaksi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bayar` int(32) NOT NULL,
  `kembali` int(32) NOT NULL,
  `id_admin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `no_nota`, `tgl_transaksi`, `bayar`, `kembali`, `id_admin`) VALUES
(1, 'T190111001', '2019-01-11 06:26:19', 2000000, 1000000, 24);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`),
  ADD KEY `id_role` (`id_role`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barang_ibfk_1` (`id_admin`);

--
-- Indexes for table `conf_app`
--
ALTER TABLE `conf_app`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conf_printer`
--
ALTER TABLE `conf_printer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_nota` (`id_nota`) USING BTREE;

--
-- Indexes for table `nota`
--
ALTER TABLE `nota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_ibfk_1` (`id_barang`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `conf_app`
--
ALTER TABLE `conf_app`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `conf_printer`
--
ALTER TABLE `conf_printer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;
--
-- AUTO_INCREMENT for table `nota`
--
ALTER TABLE `nota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=264;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
