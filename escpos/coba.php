<?php
/* Open the printer; this will change depending on how it is connected */
$connector = new WindowsPrintConnector("POS-58");
$printer = new Printer($connector);

$mode_print_A = Printer::MODE_FONT_B;

/* Information for the receipt */
$items = array(
    new item("Example item #1","x2",'450.000',"125.000"),
    new item("Another thing","x4","500.000","900.000"),
   );
$subtotal = new item('Subtotal', '300.000');

$total = new item('Total', '400.000');
/* Date is kept the same for testing */
// $date = date('l jS \of F Y h:i:s A');
$date = "Monday 6th of April 2015 02:56:25 PM";

/* Start the printer */
//$logo = EscposImage::load("resources/escpos-php.png", false);

/* Print top logo */


/* Items */
$printer -> setJustification(Printer::JUSTIFY_LEFT);
$printer -> setEmphasis(true);
$box = "".str_repeat("=", 32)."";

$printer -> textRaw($box);
 $printer -> selectPrintMode($mode_print_A);
$printer -> text(new item('','qty','harga','sub'));
foreach ($items as $item) {
    $printer -> selectPrintMode($mode_print_A);
    $printer -> text($item);
    $printer -> selectPrintMode();
}
$printer -> setEmphasis(true);
$box = "".str_repeat("=", 32)."";

$printer -> textRaw($box);
$printer -> feed();
$printer -> text($subtotal);
$printer -> text($total);
$printer -> setEmphasis(false);




/* Footer */

/* Cut the receipt and open the cash drawer */
$printer -> cut();
$printer -> pulse();

$printer -> close();

/* A wrapper to do organise item names & prices into columns */
class item
{
    private $name;
    private $price;
    private $price2;
    private $dollarSign;
    private $dollarSign2;
    private $qtySign;
    private $qty;

    public function __construct($name = '', $price = '', $qty = '', $price2='' , $qtySign = false, $dollarSign2 = false, $dollarSign = false)
    {
        $this -> name = $name;
        $this -> qty = $qty;
        $this -> price = $price;
        $this -> price2 = $price2;
        $this -> qtySign = $qtySign;
        $this -> dollarSign = $dollarSign;
        $this -> dollarSign2 =$dollarSign2;
    }

    public function __toString()
    {
        $rightCols = 1;
        $midCols = 8;
        $midCols2 = 10;
        $leftCols = 20;
        if($this -> qtySign){
            $midCols = $midCols / 5 -  $rightCols/5  ;
        }
        if($this -> dollarSign2){
            $midCols2 = $midCols2 /10 - $midCols/10 ;
        }        
        if ($this -> dollarSign) {
            $leftCols = $leftCols / 6 - $midCols / 6;
        }
        $left = str_pad($this -> name, $leftCols);
       
        $sign2 = ($this -> qtySign ? 'qty' : '');
        $sign3 = ($this -> dollarSign2 ? 'harga' : '');
        $sign = ($this -> dollarSign ? 'sub' : ''); 
        $mid = str_pad($sign2 . $this -> qty, $midCols, ' ', STR_PAD_LEFT);
        $mid2 = str_pad($sign3 . $this -> price2, $midCols2,  ' ', STR_PAD_LEFT);
        $right = str_pad($sign . $this -> price, $rightCols, ' ', STR_PAD_LEFT);
        return "$left$right$mid$mid2\n";
    }
}