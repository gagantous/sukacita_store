

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        STOK BARANG
      </h1>
    
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i>HALAMAN UTAMA</a></li>
        <li class="active">STOK</li>
      </ol>
    </section>
  <form class="form-horizontal" action="<?php echo base_url() ?>stok/delete_multiple" method="POST" >
    <!-- Main content -->
    <section class="content">
    <div class="col-md-12">
      <div class="row">
          <div class="box box-info">
            <div class="box-header">
              <!-- <h3 class="box-title">DATA STOK BARANG</h3> -->
              <?php if($nama_role == "super" || $nama_role == "admin") { ?>
              <a type="button" class="pull-right badge bg-red" role="button" href="<?php echo base_url(); ?>stok/tambah">Tambah Data </a>
              <?php } ?>

            

            <a type="button" class="pull-left badge bg-red" data-toggle="modal" data-target="#myModalstok1" role="button">Hapus</a>
            
              <!-- /.box-footer -->
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
              <table id="example1"  class="table table-bordered table-striped table-hover">
                <thead>
                <tr >
                  <th class="tutup"><input type="checkbox" id="select_all" /></th>
                  <th>No</th>
                  
                  <th>Nama Barang</th>
                  
                  <th>Satuan</th>
                  
                  <th>Harga Barang</th>
                  <th>Stock Barang</th>
                  <th>Tanggal Masuk barang</th>
                  <th>Tanggal Terakhir Update</th>
                  <?php if($nama_role == "super" || $nama_role == "admin") { ?>
                  <th align="center">Lanjut</th>
                  <?php } ?>
                </tr>
                </thead>
                <tbody>
               
              <?php 
                $no = 1;
              foreach ($gol as $stok) { 
                ?>
                <?php if($stok->id != 0) { ?>
                <tr>
                
                <td>
                <input type="checkbox" name="msg[]" value="<?php echo $stok->id; ?>"/></td>
              
                   <td><?php echo $no++; ?></td >
                  <td><?php echo $stok->nama_barang; ?></td>
                  <td><?php echo $stok->satuan; ?></td>
                  <td>Rp <?php echo  number_format($stok->harga,0,',','.')?></td>
                  <td><?php echo $stok->stok?></td>
                  <td> <?php $month_num = date('d F Y, h:i A', strtotime($stok->tgl_masuk)); echo $month_num; ?></td>
                  <td><?php $month_num = date('d F Y, h:i A', strtotime($stok->tgl_barang_diupdate)); echo $month_num; ?></td>
                  <?php if($nama_role == "super" || $nama_role == "admin") { ?>
                  <td width="100px">  
                  <div class="btn-group">
                  <a type="input" name="submit" id="submit" class="btn btn-info" role="button" href="<?php echo base_url() ?>stok/ubah/<?php echo $stok->id?>"><i class="glyphicon glyphicon-edit" style=""></i></a>

                  <button type="button" class="hap pull-left btn btn-danger" data-toggle="modal" data-id="<?php echo $stok->id ?>"  data-target="#myModalstok" role="button"><i class="glyphicon glyphicon-trash"></i></button></td>
                  <?php } ?>
                  </div>
               
                </tr>
                 <?php } ?>
                <?php  } ?>
                </tbody>
                

              </table>
              


            </div>
            <div class="modal fade modal-danger " id="myModalstok1" role="dialog" >
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <center><h3 class="modal-title"><i class="glyphicon glyphicon-exclamation-sign"style="font-size: 20px;"></i> PERINGATAN</h3></center> 
                  </div>
                  <div class="modal-body">
                    <h3><center>Apakah anda yakin ingin<br/>menghapus data-data yang sudah anda pilih  ? </h3><br/> </center>
                  </div>
                  <div class="modal-footer">
                    <button type="button" style="font-size: 20px;" class="btn btn-outline pull-left" data-dismiss="modal">Kembali</button>
                     <input type="submit" name="submit"  value="submit" class="btn btn-outline"> 
                    <!-- <button type="button" style="font-size: 20px;" onclick="hapusstoksemua()" class="btn btn-outline"><i class="glyphicon glyphicon-remove" style="font-size: 20px;"></i> Hapus </button> -->
                  </div>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
             <div class="modal fade modal-danger " id="myModalstok" role="dialog" >
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <center><h3 class="modal-title"><i class="glyphicon glyphicon-exclamation-sign"style="font-size: 20px;"></i> PERINGATAN</h3></center> 
                  </div>
                  <div class="modal-body">
                    <h3><center>Apakah anda yakin ingin menghapus ? </h3><br/> </center>
                  </div>
                  <div class="modal-footer">
                    <button type="button" style="font-size: 20px;" class="btn btn-outline pull-left" data-dismiss="modal">Kembali</button>
                     <a href="#" style="font-size: 20px;" id="modalDelete" type="button" class="btn btn-outline"><i class="glyphicon glyphicon-remove" style="font-size: 20px;"></i> Hapus </a>
                  </div>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
                      
                      
              </form>

            
            </div>
            </div>
            </div>  
    </section>
    </div>
      
            <!-- /.box-body -->
            
            
            
           <!--
           <div class="example-modal" id="myModalstok" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body">
                          <h3><center>Apakah anda yakin ? <br/></h3> </center>
                          <center><h5>Cetak Struk</h5></center>
                          <p></p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" style="font-size: 20px;" class="btn btn-outline pull-left" data-dismiss="modal">Tutup</button>
                          <button type="button" style="font-size: 20px;" onclick="hapusstok()" class=" btn btn-outline"><i class="glyphicon glyphicon-print" style="font-size: 20px;"></i> Cetak</button>
                        </div>
                      </div>
                     
                    </div>
                    
                  </div> -->
                  


         
        
          <!-- /.box -->
      
        <!-- /.row -->

     
  
     

    <!-- /.content -->
    <script type="text/javascript">
       function hapusstok(){
          location.href = '<?php echo base_url(); ?>stok/hapus_stok/<?php echo $stok->id?>';
        }
           
    </script>
   

    
    <!-- /.content -->
  <script src="<?php echo base_url() ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->

<!-- DataTables -->

<!-- page script -->
<script>
  $(function () {


    $('.hap').click(function(){
    //get cover id
    var id = $(this).data('id');
    //set href for cancel button
    $('#modalDelete').attr('href','<?php echo base_url(); ?>stok/hapus_stok/'+id);
      })
         $('#select_all').click(function(event) {
        if(this.checked) {
      // Iterate each checkbox
      $(':checkbox').each(function() {
          this.checked = true;
      });
      }
      else {
        $(':checkbox').each(function() {
              this.checked = false;
          });
      }
      });

    $("#example1").DataTable({
      "aoColumnDefs": [
           {

               "bSortable": false,
               "aTargets": ["tutup"]
           }
        ],
        "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "Semua"] // change per page values here
              ],
        "language": {
                "lengthMenu": "Tampilkan _MENU_ data",
                "paginate": {
                  "previous":"Prev",
                  "next": "Next",
                  "last": "Last",
                  "first": "First"
                }
              },


    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });


</script>
  <!-- /.content-wrapper -->

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->

        <!-- /.col (RIGHT) -->
      


<!-- ./wrapper -->
