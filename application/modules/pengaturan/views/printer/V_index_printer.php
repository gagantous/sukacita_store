  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo strtoupper(@$nama_modul) ?>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo $this->session->flashdata('alert'); ?>
      <div class="row">
      <div class="col-md-12">
          <div class="box box-info">

            <div class="box-header">
              <h3 class="box-title">Pengaturan Aplikasi </h3>
            </div>


            <div class="box-body">
              <div class="callout callout-info">
                <h4>I am an info callout!</h4>

                <p>Follow the steps to continue to payment.</p>
              </div>
            </div>
             <div class="box-footer">
              
            </div>
          </div>
        <!-- /.col -->
        </div>
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Pengaturan Printer </h3>
            </div>
            <form class="form-horizontal">
            <div class="box-body">
              <div class="form-group">
                    <label for="Text" class="col-sm-4 control-label">Nama Printer</label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control" id="namabarang" name="namabarang" placeholder="Nama Barang">
                    </div>
                  </div>
            </div>
          </form>
            <div class="box-footer">
              
            </div>
          </div>
        <!-- /.col -->
        </div>

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>