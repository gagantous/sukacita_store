<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_stok');
		$ses = $this->session->userdata('sukacita');
		if(!$ses){
			redirect(base_url('login'));
		}
	}

	public function index(){
		$sess = $this->session->userdata('sukacita');
		$data['nama'] = $sess['nama'];
		$data['nama_role'] = $sess['nama_role'];
		$data['id_admin'] = $sess['id_admin'];
		$data['gol'] = $this->M_stok->get_stok()->result();
		//$data['gol'] = $this->M_stok->get_stok()->result();
		$this->load->view('header',$data);
		$this->load->view('sidebar');		
		$this->load->view('V_index_stok',$data);
		$this->load->view('footer');

	}

	public function ubah($id = null)
	{
		$sess = $this->session->userdata('sukacita');
		$data['nama'] = $sess['nama'];
		$data['nama_role'] = $sess['nama_role'];
		
		$data['act'] = 'stok';
		
		$submit = $this->input->post('submit');

		if($submit){

			$xid['id'] = $this->input->post('id');
			$namabarang = $this->input->post('namabarang');
			
			$satuan = $this->input->post('satuan');
			$harga = $this->input->post('harga');
			$stok = $this->input->post('stok');
			$id_admin = $this->input->post('id_admin');

			$last =  $this->db->query("select * from barang where id=".$xid['id']);
				$row = $last->row();

				$min_barang = $row->stok;
				$sb_kurang = $stok-$min_barang;
				$sejumlah = '';
				$status = '';

				if($sb_kurang<0){

						$sejumlah = abs($sb_kurang);
						$status = "Pengurangan jumlah barang dari stok";
						
				}

				else if($sb_kurang<$min_barang && $sb_kurang>0 ){

					
						$sejumlah = $sb_kurang;
						$status = "Penambahan jumlah barang dari stok";
						
				}
				else{
					$sejumlah = $sb_kurang;
					$status = "Perubahan Informasi data barang";
				}


				$stok_arr_rtambah = array(

						'id_barang' => $row->id,
						'sejumlah' => $sejumlah,
						'status' => $status
						);

				$this->M_stok->status($stok_arr_rtambah);
							
				$stok_arr = array(
						
						'nama_barang' => $namabarang,
						'satuan' => $satuan,
						'harga' => $harga,
						'stok' => $stok,
						'id_admin' => $id_admin
				);


			$this->M_stok->update_stok($stok_arr, $xid);

			redirect(base_url("stok"));
		}
		else{

			$cek = $this->M_stok->cek_stok_id($id);
			if($cek){
				$stok['data'] = $this->M_stok->get_stok_id($id);
				$this->load->view('header',$data);
				$this->load->view('V_edit_stok', $stok);
				$this->load->view('footer');
			}else{
				redirect(base_url().'error');
			}
			
		}
	}
	
	/* public function edit(){
		$this->load->view('header');
		$this->load->view('V_edit_stok');
		$this->load->view('footer');

	}
	*/

	public function tambah(){
		/*$this->load->view('header');
		$this->load->view('V_tambah_stok');
		$this->load->view('footer');
		*/
		$sess = $this->session->userdata('sukacita');
		$data['nama'] = $sess['nama'];
		$data['nama_role'] = $sess['nama_role'];
			$this->load->view('header',$data);
			$this->load->view('V_tambah_stok');
			$this->load->view('footer');
		}

	public function tambah_b(){
		$sess = $this->session->userdata('sukacita');
		$data['id_admin'] = $sess['id_admin'];
		$data['nama'] = $sess['nama'];
		$data['nama_role'] = $sess['nama_role'];
		$data['act'] = 'stok';
		$submit = $this->input->post('submit');
		
		if($submit){

			$namabarang = $this->input->post('namabarang');
			$satuan = $this->input->post('satuan');
			$harga = $this->input->post('harga');
			$stok = $this->input->post('stok');
							
				$stok_arr = array(
						
						'nama_barang' => $namabarang,
						'satuan' => $satuan,
						'harga' => $harga,
						'stok' => $stok,

						'id_admin' => $sess['id_admin']
				);
				$this->M_stok->input_stok($stok_arr);

				$last =  $this->db->query("select * from barang order by id desc limit 1");
				$row = $last->row();
				
				$status_stok = array(

					'id_barang' => $row->id,
					'sejumlah' => $stok,
					'status' => "Barang baru"
					);

				$stat = $this->M_stok->status($status_stok);



				redirect(base_url("stok"));
			}

	}

	public function hapus_stok($id = null){
	$data['act'] = 'stok';
		$data['aksi'] = 'hapus';

		$cek = $this->M_stok->cek_stok_id($id);

			$data['cek'] = $this->M_stok->cek_stok_id($id);
			
			$riw_hap = array(
				'id_barang' => $data['cek']['id'],
				 'sejumlah' =>  $data['cek']['stok'],
				 'status' => "Barang dihapus"
				);
			$stat = $this->M_stok->status($riw_hap);


			$this->M_stok->hapus($id);

			redirect(base_url("stok"));
		}

	public function delete_multiple() {
			$this->load->model('M_stok');
			$this->M_stok->remove_checked_stok();
			redirect('stok');
			
		}
}
