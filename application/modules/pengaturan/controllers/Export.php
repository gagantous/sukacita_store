<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export extends CI_Controller {


	function __construct(){
		parent::__construct();
		$this->load->model('M_export');
		$ses = $this->session->userdata('sukacita');
		if(!$ses){
			redirect(base_url('login'));
		}
	}

	public function index(){
		$sess = $this->session->userdata('sukacita');
		$data['nama'] = $sess['nama'];
		$data['nama_role'] = $sess['nama_role'];
		$data['id_admin'] = $sess['id_admin'];
		$data['gol'] = $this->M_export->get_stok()->result();
		//$data['gol'] = $this->M_export->get_stok()->result();
		$this->load->view('header',$data);
		$this->load->view('sidebar');
		$this->load->view('export/V_index_export',$data);
		$this->load->view('footer');

	}

}
