<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct(){
		parent::__construct();
		//$this->load->model('M_siswa');
		$this->load->helper('url');
		$this->load->model('M_riwayat_transaksi');
		$ses = $this->session->userdata('sukacita');
		if(!$ses){
			redirect(base_url('login'));
		}

	}

	public function index(){
		$sess = $this->session->userdata('sukacita');
		$data['nama'] = $sess['nama'];
		$data['nama_role'] = $sess['nama_role'];
		$data['aktif'] = "dashboard";
		$data['menu_modul'] = "dashboard";
		$data['title'] = "Dashboard";
		$data['grafik'] = $this->M_riwayat_transaksi->x();
		$data['grafik2']  = $this->M_riwayat_transaksi->x();
		$data['gol'] = $this->M_riwayat_transaksi->get_riwayat_transaksi();
		$data['gol2'] = $this->M_riwayat_transaksi->get_riwayat_transaksi2();
		$this->load->view('header',$data);
		$this->load->view('sidebar');
		$this->load->view('V_index_dashboard',$data);
		$this->load->view('footer',$data);

	}

	public function x2(){

		$data['grafik'] = $this->M_riwayat_transaksi->x();
		echo json_encode($data);
	}

	
}
