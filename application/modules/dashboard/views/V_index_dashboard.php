

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo strtoupper(@$nama_modul) ?>
        <small></small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- AREA CHART -->
          <div class="box box-info">
            <div class="box-header with-border">
            <?php 
            $tgl = date('Y-m-d');
            $yrdata = strtotime($tgl);?>
              <h3 class="box-title">GRAFIK PENJUALAN BULANAN : <?php echo date('F', $yrdata); ?></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div id="chart">
                <!-- <canvas id="lineChart" style="height:250px"></canvas> -->
              </div>
            </div>
          </div>
            <div class="row">
              <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Report kemarin</span>
                    <?php
                    foreach ($gol2 as $gol2) {
                      echo date('Y-m-d', strtotime("-1 day"));
                     ?>
                    <span class="info-box-number">Rp. <?php echo number_format(($gol2->total),0,',','.') ?></span>
                    <?php } ?>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              
              <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-green"><i class="fa fa-flag-o"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">report hari ini</span>
                    <?php
                    foreach ($gol as $gol) {
                      echo date('Y-m-d');
                     ?>
                    
                    <span class="info-box-number" >Rp. <?php echo number_format(($gol->total),0,',','.') ?></span>

                    <?php } ?>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-flag-o"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">report hari ini</span>
                      <?php  
                       foreach ($grafik as $grafik => $nama ) { 
                        echo $grafik;
                    } 
                  ?>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                      <!-- /.info-box -->
              </div>
             </div>
      <!-- /.box-body -->
            </div>
          </div>
    </section>
  </div>
  <script type="text/javascript">
    <?php 
    $list=array();
    $month = date('m');
    $year = date('y');

    for($d=1; $d<=31; $d++)
    {
        $time=mktime(12, 0, 0, $month, $d, $year);          
        if (date('m', $time)==$month)       
            $list[]=date('d', $time);
    }

    ?>
    Highcharts.chart('chart', {

    title: {
        text: 'TOTAL PENJUALAN BULAN : <?php echo date('F', $yrdata);?> <?php echo date("Y"); ?>'
    },

   
    xAxis: {
      type: 'datetime',
      min: Date.UTC(<?php echo date('Y')?>, <?php echo date('m',strtotime("-1 months"))?>, 1),
      max: Date.UTC(<?php echo date('Y')?>, <?php echo date('m',strtotime("-1 months"))?>, <?php echo date("t") ?>),
      // tickInterval: 24 * 3600 * 1000, // 1 day
      labels: {
        step: 1,
        style: {
          fontSize: '13px',
          fontFamily: 'Arial,sans-serif'
        }
      }
    },
    yAxis: {
        title: {
            text: 'Number of Employees'
        }
    },

    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },
    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 2010
        }
    },
    // plotOptions: {
    //     series: {
    //         label: {
    //             connectorAllowed: false
    //         },
    //     }
    // },

    series: [{
        name: 'Total Penjualan',
        data: [<?php foreach ($grafik2 as $total) { echo $total->total.","; } ?>]
    }],


    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});
  </script>
  