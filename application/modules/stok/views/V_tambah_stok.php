  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo strtoupper(@$nama_modul) ?>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-primary">
            <div class="box-header with-border">
                  <h3 class="box-title"><?php ucwords(@$nama_sub_modul) ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="<?php echo base_url().'stok/tambah_b'?>" method="POST" enctype="multipart/form-data">
                <div class="box-body">
                
                  <div class="form-group">
                    <label for="Text" class="col-sm-4 control-label">Nama Barang <i class="text-red">*</i></label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" id="namabarang" name="namabarang" placeholder="Nama Barang" required>
                    </div>
                  </div>
        
      				      <div class="form-group">
                        <label for="Text" class="col-sm-4 control-label">Satuan</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="satuan" name="satuan" placeholder="Satuan">
                        </div>
                      </div>
      				      <div class="form-group">
                        <label for="Text" class="col-sm-4 control-label">Harga <i class="text-red">*</i></label>
                        <div class="col-sm-6">
                          <input type="number" min="0" class="form-control" id="harga" name="harga" placeholder="Harga" required>
                        </div>
                      </div>
      				      <div class="form-group">
                        <label for="Text" class="col-sm-4 control-label">Stok</label>
                        <div class="col-sm-6">
                          <input type="number" class="form-control" id="stok" name="stok" placeholder="Stok">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                        </div>
                      </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                      <a href="<?php echo base_url() ?>stok" class="btn btn-danger pull-left">Kembali</a>
                     <!-- <button type=""  class="btn btn-default">Batal</button>  -->
                      <input type="submit" id="submit" name="submit" class="btn btn-info pull-right" value="Tambah" > 
                    <!-- /.box-footer -->
                  </form>
            </div>
          </div>
        </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  