  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo strtoupper(@$nama_modul) ?>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-primary">
            <form class="form-horizontal" action="<?php echo base_url().'stok/ubah'?>" method="POST" enctype="multipart/form-data">
            <div class="box-header with-border">
                <!-- <div>
                        <h3 class="box-title"><?php echo ucwords(@$nama_modul) ?></h3>
                </div> -->
            </div>
            <div class="box-body">
            
              <?php 
                foreach ($data as $stok){ 
              ?>
                <input type="hidden"  name="id" value="<?php echo $stok->id; ?>">
                <div class="form-group">
                  <label for="Text" class="col-sm-4 control-label">Nama Barang</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="namabarang" name="namabarang" placeholder="Nama Barang" value="<?php echo $stok->nama_barang; ?>">
                  </div>
                </div>
                    <div class="form-group">
                        <label for="Text" class="col-sm-4 control-label">Satuan</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="satuan" name="satuan" placeholder="Satuan" value="<?php echo $stok->satuan; ?>">
                        </div>
                      </div>
                    <div class="form-group">
                        <label for="Text" class="col-sm-4 control-label">Harga</label>
                        <div class="col-sm-6">
                          <input type="number" min="0" class="form-control" id="harga" name="harga" placeholder="Harga" value="<?php echo $stok->harga; ?>">
                        </div>
                      </div>
                    <div class="form-group">
                        <label for="Text" class="col-sm-4 control-label">Stok</label>
                        <div class="col-sm-6">
                          <input type="number" class="form-control" id="stok" name="stok" placeholder="Stok" value="<?php echo $stok->stok; ?>">
                        </div>
                      </div>
                      <input type="hidden"  name="id_admin" value="<?php echo $stok->id_admin; ?>">
                
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                        </div>
                      </div>
                    

                     <?php } ?>
                     </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                      <a href="<?php echo base_url() ?>stok" class="btn btn-danger pull-left">Kembali</a>
                     <!-- <button type=""  class="btn btn-default">Batal</button>  -->
                      <input type="submit" id="submit" name="submit" class="btn btn-info pull-right" value="Edit" > 
                     </div>
                 </form>
              </div>
          </div>
        </div>
    </section>
          <!-- /.content -->
</div>
  <!-- /.content-wrapper -->

   
  