<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_stok');
		$ses = $this->session->userdata('sukacita');
		if(!$ses){
			redirect(base_url('login'));
		}
	}

	public function index(){
		$sess = $this->session->userdata('sukacita');
		$data['nama'] = $sess['nama'];
		$data['nama_role'] = $sess['nama_role'];
		$data['id_admin'] = $sess['id_admin'];
		$data['gol'] = $this->M_stok->get_stok()->result();
		$data['aktif'] = 'stok';
		$data['title'] = "MANAJEMEN STOK";
		$data['nama_modul'] = "MANAJEMEN STOK";
		//$data['gol'] = $this->M_stok->get_stok()->result();
		$this->load->view('header',$data);
         $this->load->view('sidebar');
		$this->load->view('V_index_stok',$data);
		$this->load->view('footer');

	}

	public function ubah($id = null)
	{
		$sess = $this->session->userdata('sukacita');
		$data['nama'] = $sess['nama'];
		$data['nama_role'] = $sess['nama_role'];
		$data['nama_modul'] = "UBAH STOK";
		$data['act'] = 'stok';
		$data['aktif'] = 'stok';
		$data['title'] = "UBAH STOK";
		
		$submit = $this->input->post('submit');

		if($submit){

			$xid['id'] = $this->input->post('id');
			$namabarang = $this->input->post('namabarang');
			
			$satuan = $this->input->post('satuan');
			$harga = $this->input->post('harga');
			$stok = $this->input->post('stok');
			$id_admin = $this->input->post('id_admin');

			$last =  $this->db->query("select * from barang where id=".$xid['id']);
				$row = $last->row();

				$min_barang = $row->stok;
				$sb_kurang = $stok-$min_barang;
				$sejumlah = '';
				$status = '';

				if($sb_kurang<0){

						$sejumlah = abs($sb_kurang);
						$status = "Pengurangan jumlah barang dari stok";
						
				}
				else if($sb_kurang<$min_barang && $sb_kurang>0 ){

					
						$sejumlah = $sb_kurang;
						$status = "Penambahan jumlah barang dari stok";
						
				}
				else{
					$sejumlah = $sb_kurang;
					$status = "Perubahan Informasi data barang";
				}


				$stok_arr_rtambah = array(

						'id_barang' => $row->id,
						'sejumlah' => $sejumlah,
						'status' => $status
						);

				$stok_arr = array(
						
						'nama_barang' => $namabarang,
						'satuan' => $satuan,
						'harga' => $harga,
						'stok' => $stok,
						'id_admin' => $id_admin
				);

				$this->db->trans_start(); # Starting Transaction
				$this->db->trans_strict(FALSE);
				$this->M_stok->status($stok_arr_rtambah);
				$this->M_stok->update_stok($stok_arr, $xid);
				$this->db->trans_complete();
				if ($this->db->trans_status() === FALSE) {
				    # Something went wrong.
				    $this->db->trans_rollback();
				    $this->session->set_flashdata('alert','
					<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                <h4><i class="icon fa fa-ban"></i> GAGAL MENGUBAH DATA!</h4>
                Proses '.$status.' gagal dilakukan pada stok <b>'.strtoupper($namabarang).'</b> dikarenakan terdapat masalah pada database.</br> Silahkan ulangi proses data tersebut. Jika error ini terus muncul, silahkan hubungi administrator!
              </div>
					');
				} 
				else {
				    # Everything is Perfect. 
				    # Committing data to the database.
				    $this->db->trans_commit();
				   $check = $this->session->set_flashdata('alert','
					<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                <h4><i class="icon fa fa-check"></i> BERHASIL MENGUBAH STOK!</h4>
                '.$status.' berhasil dilakukan pada barang <b>'.strtoupper($namabarang).'<b>!
              </div>
					');
				}

			redirect(base_url("stok"));
		}
		else{

			$cek = $this->M_stok->cek_stok_id($id);
			if($cek){
				$stok['data'] = $this->M_stok->get_stok_id($id);
				$this->load->view('header',$data);
     			$this->load->view('sidebar');
				$this->load->view('V_edit_stok', $stok);
				$this->load->view('footer');
			}else{
				redirect(base_url().'error');
			}
			
		}
	}
	
	/* public function edit(){
		$this->load->view('header');
		$this->load->view('V_edit_stok');
		$this->load->view('footer');

	}
	*/

	public function tambah(){
		/*$this->load->view('header');
		$this->load->view('V_tambah_stok');
		$this->load->view('footer');
		*/
		$sess = $this->session->userdata('sukacita');
		$data['nama'] = $sess['nama'];
		$data['nama_role'] = $sess['nama_role'];
		$data['nama_modul'] = "TAMBAH STOK";
		$data['title'] = "TAMBAH STOK";
		$data['aktif'] = 'stok';
			$this->load->view('header',$data);
         $this->load->view('sidebar');
			$this->load->view('V_tambah_stok');
			$this->load->view('footer');
		}

	public function tambah_b(){
		$sess = $this->session->userdata('sukacita');
		$data['id_admin'] = $sess['id_admin'];
		$data['nama'] = $sess['nama'];
		$data['nama_role'] = $sess['nama_role'];
		$data['act'] = 'stok';
		$submit = $this->input->post('submit');
		
		if($submit){

			$namabarang = $this->input->post('namabarang');
			$satuan = $this->input->post('satuan');
			$harga = $this->input->post('harga');
			$stok = $this->input->post('stok');
							
				$stok_arr = array(
						
						'nama_barang' => $namabarang,
						'satuan' => $satuan,
						'harga' => $harga,
						'stok' => $stok,

						'id_admin' => $sess['id_admin']
				);
				$this->db->trans_start();
				$this->db->trans_strict(FALSE);
				$this->M_stok->input_stok($stok_arr);
				$last =  $this->db->query("select * from barang order by id desc limit 1");
				$row = $last->row();
				$status_stok = array(

					'id_barang' => $row->id,
					'sejumlah' => $stok,
					'status' => "Barang baru"
					);

				$this->M_stok->status($status_stok);
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					$this->session->set_flashdata('alert','
						<div class="alert alert-danger alert-dismissible">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
	                <h4><i class="icon fa fa-ban"></i> GAGAL MENAMBAH STOK BARU!</h4>
	                Gagal menambah stok baru dikarenakan terdapat masalah pada jaringan atau sistem. </br> Silahkan ulangi lagi proses penginputan/hubungi pihak administrator!
	              </div>
						');
					
				}else{
					$this->db->trans_commit();
					$this->session->set_flashdata('alert','
					<div class="alert alert-success alert-dismissible">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
	                <h4 style="font-weight: bold;"><i class="icon fa fa-check"></i> SUKSES MENAMBAH STOK BARU!</h4>
	                Stok baru berhasil di tambahkan!
	              	</div>
						');
					
				};
				redirect(base_url("stok"));
			}

	}

	public function hapus_stok($id = null){
		$data['act'] = 'stok';
		$data['aktif'] = 'stok';
		$data['aksi'] = 'hapus';

		$cek = $this->M_stok->cek_stok_id($id);

			$data['cek'] = $this->M_stok->cek_stok_id($id);
			
			$riw_hap = array(
				'id_barang' => $data['cek']['id'],
				 'sejumlah' =>  $data['cek']['stok'],
				 'status' => "Barang dihapus"
				);
			$stat = $this->M_stok->status($riw_hap);


			$this->M_stok->hapus($id);
			$check = $this->db->affected_rows();
			if($check){
				$this->session->set_flashdata('alert','
					<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                <h4 style="font-weight: bold;"><i class="icon fa fa-check"></i> SUKSES MENGAHPUS DATA!</h4>
                BERHASIL MENGHAPUS DATA!
              </div>
					');
			}else{
				$this->session->set_flashdata('alert','
					<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                <h4><i class="icon fa fa-ban"></i> GAGAL MENGHAPUS DATA!</h4>
                Gagal Menghapus data dikarenakan terdapat masalah pada jaringan atau sistem. </br> Silahkan ulangi lagi proses penginputan/hubungi pihak administrator!
              </div>
					');
			};

			redirect(base_url("stok"));
		}

	public function delete_multiple() {
			$this->load->model('M_stok');
			$delete = $this->input->post('msg');
			$delcount = count($delete);
			$data = $this->M_stok->remove_checked_stok();
			if($data['total'] === $data['deleted']){
				$this->session->set_flashdata('alert','
					<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4 style="font-weight: bold;"><i class="icon fa fa-check"></i> SUKSE MENGAHPUS DATA!</h4>
                Sukses Menghapus '.$data['total'].' dari '.$data['deleted'].' data yang dipilih!
              </div>
					');
			}else if ($data['total'] > $data['deleted']) {
				$this->session->set_flashdata('alert','
					<div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4 style="font-weight: bold;"><i class="icon fa fa-warning"></i> PERINGATAN, TERJADI KESALAHAN!</h4>
                Hanya dapat menghapus '.$data['total'].' dari '.$data['deleted'].' data yang dipilih karena adanya gangguan pada data yang tidak valid, silahkan, hapus satu persatu data tersebut.
              </div>
					');

			}else{
				$this->session->set_flashdata('alert','
					<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> GAGAL MENGHAPUS DATA!</h4>
                Gagal Menghapus data multiple dikarenakan terdapat masalah pada jaringan atau sistem silahkan ulangi lagi proses penginputan/hubungi pihak administrator!
              </div>
					');
			}
			redirect('stok');
			
		}
}
