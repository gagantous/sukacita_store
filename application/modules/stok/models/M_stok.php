<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_stok extends CI_Model {
	
	public $tabel = 'barang';


	public function get_stok()
	{
		/* $this->db->select('*');
		$this->db->from('barang b');
		$this->db->join('kategori k','b.id_kategori = k.id');
		$this->db->join('stok a','b.id_stok = a.id');
		*/
		
		$query = $this->db->get('barang');
		return $query;
	}



	public function input_stok($data){
		$this->db->insert($this->tabel, $data);

	}

	public function update_stok($data, $id)
    {
        $query = $this->db->update($this->tabel, $data, $id);
    }


	public function edit_stok($data, $table){
		$res = $this->db->get_where($table, $where);
		return $res;
	}

	function get_last_id(){
		$this->db->order_by("id","desc");
		$this->db->select('*');
		$this->db->from('barang');
		$query=$this->db->get();
		return $query->result();
	}

	function status($data){
		$ber = $this->db->insert('status', $data);
		return $ber;

	}

	 public function cek_stok_id($id){
    	$this->db->where('id', $id);
    	$query = $this->db->get($this->tabel);

    	if ($query->num_rows() > 0)
        { return $query->row_array();
        }
        else {return NULL;}
    }

    public function get_stok_id($id){
		$this->db->where('id', $id);
		$query = $this->db->get($this->tabel);

		return $query->result();
	}

	public function hapus($id){
    	$this->db->where('id', $id);
    	$this->db->delete($this->tabel);
    	
    }

  	public function remove_checked_stok() {
	
			$delete = $this->input->post('msg');
			$hit = 0;
			for ($i=0; $i < count($delete) ; $i++) { 
				$this->db->where('id', $delete[$i]);
				$this->db->delete('barang');
				$check = $this->db->affected_rows();
				if($check){
					$hit = $hit +1;
				};				
			}

			return array(
				'total' => count($delete), 
				'deleted' => $hit
			);
	}
}
