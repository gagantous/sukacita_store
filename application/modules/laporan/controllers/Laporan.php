<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct(){
		parent::__construct();
		//$this->load->model('M_siswa');
		//$this->load->helper('url');
	}

	public function index(){
		//$data['gol'] = $this->M_siswa->get_siswa()->result();
		$this->load->view('header');
		$this->load->view('V_index_dashboard');
		$this->load->view('footer');

	}

	
}
