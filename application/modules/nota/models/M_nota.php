<?php

Class M_nota extends CI_Model {
	private $primary_key = 'id';
	private $table_name	= 'barang';

	public function get_nota(){
		
		return $this->db->get($this->table_name);
	}

	public function get_no()
	{
		$query = "
			SELECT CONVERT(SUBSTRING(nota,-3),UNSIGNED INTEGER) AS akhir, SUBSTRING(nota, 2, 4) AS bln
			FROM nota 
			ORDER BY id DESC ";
		
		$src = $this->db->query($query);
		
		if ($src->num_rows() != 0) {
			$kode = ($src->row()->bln != date('ym')) ? '0' : $src->row()->akhir;
		} else {
			$kode = '0';
		}
		
		return $kode;
	}
	

	public function input_nota($data,$table){
		$this->db->insert($table,$data);

	}

	public function edit_nota($data, $table){
		$res = $this->db->get_where($table, $where);
		return $res;
	}

	public function get_id_barang($barangs){
		
	$this->db->where('id',$barangs);
	$query = $this->db->get('barang');

	return $query->result();

}
	public function get_by_id($id)
	{
	  
	  	$this->db->where($this->primary_key,$id); 
	  
	  	return $this->db->get($this->table_name)->row();
	
	}

	function status($data){
		$ber = $this->db->insert('status', $data);
		return $ber;

	}

	public function lastdata()
	{
		$this->db->order_by("id","desc");
		$this->db->select('*');
		$this->db->from('nota');
		$this->db->limit('1');
		$query=$this->db->get();
		return $query->result("array");


	}

	public function order($data)
	{
		$ber = $this->db->insert('nota', $data);
		return $ber;
	}

	function order_detail($data){
		$ber = $this->db->insert('detail_nota', $data);
		return $ber;

	}
}


