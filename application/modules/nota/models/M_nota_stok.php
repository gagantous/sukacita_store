<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_nota_stok extends CI_Model {
	
	public $tabel = 'barang';


	public function get_nota()
	{
		/* $this->db->select('*');
		$this->db->from('barang b');
		$this->db->join('kategori k','b.id_kategori = k.id');
		$this->db->join('nota a','b.id_nota = a.id');
		*/
		
		$query = $this->db->get('barang');
		return $query;
	}



	public function input_nota($data){
		$this->db->insert($this->tabel, $data);

	}

	public function update_nota($data, $id)
    {
        $query = $this->db->update($this->tabel, $data, $id);
    }


	public function edit_nota($data, $table){
		$res = $this->db->get_where($table, $where);
		return $res;
	}

	 public function cek_nota_id($id){
    	$this->db->where('id', $id);
    	$query = $this->db->get($this->tabel);

    	return $query->num_rows();
    }

    public function get_nota_id($id){
		$this->db->where('id', $id);
		$query = $this->db->get($this->tabel);

		return $query->result();
	}

	public function hapus($id){
    	$this->db->where('id', $id);
    	$this->db->delete($this->tabel);
    }
}
