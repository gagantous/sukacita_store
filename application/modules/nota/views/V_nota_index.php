<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       HALAMAN TRANSAKSI
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?> "><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Transaksi</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

    <div class="box box-default">
      <div class="box-header with-border">
        <div class="">
          <b><h3 class="box-title">NOMOR NOTA :    
        </div>
      </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
            <form class="form-horizontal" >

              

              <div class="box-body">
              <div class="form-group">

              
                  <label for="barangs" style="font-size: 15px;"  class="col-sm-2 control-label">ID/Barang</label>

                  <div class="col-sm-10">
                    <input type="text" style="height: 50px; font-size: 25px;" onkeyup="get_detail(this.value)" onchange="get_detail(this.value)" class="form-control reset" id="barangs" name="barangs" list="barang" placeholder="ID/Barang"/>
                    <datalist id="barang" >
                    <?php foreach($barang as $barang) { ?> 
                    <option value="<?php echo $barang->id; ?>"><?php echo $barang->nama_barang ; ?></option>
                    <?php } ?>
                    </datalist>
                  </div>
                </div>
                <div class="form-group">
                  <label style="font-size: 15px;" for="satuan" class="col-sm-2 control-label">Nama Barang</label>

                  <div class="col-sm-10">
                    <input type="text" style="height: 50px; font-size: 25px;" class="form-control reset" id="nama_barang" name="nama_barang" placeholder="Nama barang" value="">
                  </div>
                </div>              
                <div class="form-group">
                  <label style="font-size: 15px;" for="harga" class="col-sm-2 control-label">Harga</label>

                  <div class="col-sm-10">
                  <div class="input-group">
                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
                    <span  style="font-size: 25px;" class="input-group-addon">Rp</span>
                    <input type="text" style="height: 50px; font-size: 25px;" class="form-control reset" id="harga" name="harga" placeholder="Harga barang..." value="">
                  </div>
                  </div>
                </div>


                <div class="form-group">
                  <label style="height: 50px; font-size: 15px;" for="stok" class="col-sm-2 control-label">Quantity</label>

                  <div class="col-sm-3">
                    <input type="number" onchange="subTotal(this.value)" style="height: 50px; font-size: 25px;" class="form-control reset" id="stok2" name="stok2" min="0" value="0">
                  </div>

                  <div class="col-sm-2">
                   <label style="height: 50px; font-size: 25px;" for="harga" class="control-label">Stok:</label>
                   </div>
                   <div class="col-sm-3">
                   <input type="text" style="height: 50px; font-size: 25px;" readonly class="form-control reset" id="stok" name="stok"  value="">
                  </div>

                </div>
                <div class="form-group">
                  <label for="subtotal"  style="font-size: 15px;" class="col-sm-2 control-label">Subtotal</label>

                  <div class="col-sm-10">
                  <div class="input-group">
                  <span  style="font-size: 25px;" class="input-group-addon">Rp</span>
                    <input type="text" style="height: 50px; font-size: 25px;" class="form-control reset" disabled="" id="subtotal" name="subtotal" placeholder="Sub-Total Harga...">
                  </div>
                  </div>
                </div>
                </form>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
            <div class="col-md-offset-8">
                <button style="font-size: 25px;" type="button" class="btn btn-primary" 
                id="tambah" >
                  <i class="fa fa-cart-plus" style="font-size: 20px;" ></i> TAMBAH</button>

                  <!-- <input id="id_barangs" value="" /> -->
            </div>
          </div>
             </form> 
              <!-- /.form-group -->
            </div>
            <div class="row">
            <div class="col-md-6">
            

<style type="text/css">
    #total
{
   border: none;
   background-color: transparent;

}
#stok{

  border: none;
   background-color: transparent;
  font-weight: bold;
}


#kembali{
  border: none;
  background-color: transparent;
}

</style>



       <form action="<?php echo base_url().'transaksi/selesai'?>" method="POST" enctype="multipart/form-data">      
              <div class="box-body">
             <style type="text/css">

             </style>
                <input type="text" id="nomornotanya" name="nomornotanya" value="" />
                <div class="form-group">
                  <label for="total" style=" font-size: 20px;" class="col-sm-4 control-label">Total (Rp) :</label>

                  <div class="col-sm-6">
                    <input type="text" style="height: 50px; font-size: 20px;" readonly="readonly" class="form-control" id="total" name="total" placeholder="Total harga" value="" />
                  </div>
                </div>
                <div class="form-group">
                  <label for="bayar" style=" font-size: 20px;"  class="col-sm-4 control-label">Bayar (Rp) :</label>

                  <div class="col-sm-6">
                    <input type="text" style="height: 50px; font-size: 20px;" class="form-control" onkeypress="return event.charCode >= 48 && event.charCode <= 58" oninput="showKembali(this.value)" onkeypress maxlength="15" id="bayar" name="bayar" placeholder="Masukkan nominal" />
                  </div>
                </div>
                <div class="form-group">
                  <label for="kembali" style=" font-size: 20px;"  class="col-sm-4 control-label">Kembali (Rp) :</label>


                  <div class="col-sm-6">
                    <input type="text" style="height: 50px; font-size: 20px;" readonly="readonly"  class="form-control" id="kembali" name="kembali" placeholder="Kembalian" />
                  </div>
                </div>
                <div class="form-group">

            <div class="modal fade modal-info " id="myModal2" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
              </div>
              <div class="modal-body">
                <h3><center>Apakah anda yakin ? <br/></h3> </center>
                <center><h5>Cetak Struk</h5></center>
                <p></p>
              </div>
              <div class="modal-footer">
                <button type="button" style="font-size: 20px;" class="btn btn-outline pull-left" data-dismiss="modal">KEMBALI</button>
                <input type="submit"  name="submit" value="CETAK" style="font-size: 20px;" class=" btn btn-outline">
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

             <div class="col-sm-2 col-md-offset-3">
                <button style="font-size: 20px;" type="button" class="btn btn-danger btn-lg" 
                id="batal" name="batal" data-toggle="modal" data-target="#myModal" disabled="disabled">
                  <i class="glyphicon glyphicon-remove"style="font-size: 20px;" ></i> BATAL</button>
            </div>
             <div class="col-md-offset-1 col-sm-1 " >
                <button style="font-size: 20px;" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal2" 
                id="selesai" disabled="disabled" >
                SELESAI <i class="glyphicon glyphicon-ok" style="font-size: 20px;"></i></button>
              </div>
            </div>
        
                </form>
              </div>
              <!-- /.form-group -->
              
              <!-- /.form-group -->
            </div>


            <!-- /.col -->
            
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>

<style type="text/css">
.modal {
  text-align: center;
}
  @media screen and (min-width: 768px) { 
  .modal:before {
    display: inline-block;
    vertical-align: middle;
    content: " ";
    height: 100%;
  }
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>
       
        <!-- /.modal -->
      

        <div class="modal fade modal-danger" id="myModal" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
               <center><h3 class="modal-title"><i class="glyphicon glyphicon-exclamation-sign"style="font-size: 20px;"></i> PERINGATAN</h3></center>
              </div>
              <div class="modal-body">
                <h3><center>Apakah anda yakin ingin membatalkan transaksi ?<br/></h3> </center>
                <center><h5>*semua transaksi akan dihapus pada transaksi kali ini</h5></center>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Batal</button>
                <button type="button" onclick="batal();" class="btn btn-outline">Batal transaksi</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
      

      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->
          <?php ?>
    
      <script type="text/javascript">
        function batal(){
          location.href = '<?php echo base_url() ?>transaksi/batal';
        }
      
        function selesai(){
          location.href = '<?php echo base_url().'transaksi/selesai/'.$nomor;?>';
        }
      </script>
          
            <!-- /.box-header -->
            <div class="box-body">
              <table style="font-size: 20px;"id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Id barang</th>
                  <th>Nama Barang</th>
                  <th>Harga</th>
                  <th>Qty</th>
                  <th>Sub total</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody id="body-table"></tbody>
              </table>


            </div>


            <!-- /.box-body -->
      
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  <script type="text/javascript">
  
  $('#tambah').click(function(){


    var id_barang = $('#barangs').val();
    var stk = $('#stok').val();
    var qty = $('#stok2').val();
        if (id_barang == '') {
          $('#barangs').focus();
        }else if(qty == ''){
          $('#stok2').focus();
        }else if(qty > stk){
          $('$stok2').focus();
        }else{

          var id_barangs = $('#id_barangs').val();
          if(id_barangs == ''){
            $('#id_barangs').val($('#barangs').val());
          }else{
            $('#id_barangs').val(id_barangs+','+$('#barangs').val());
          }

    $.ajax({
        type: "POST",
        url: "<?php echo base_url() ?>transaksi/addbarang", // ke script controller
        data: { 
                  id_barang:$('#barangs').val(),
                  satuan:$('#satuan').val(),
                  nama_barang:$('#nama_barang').val(),
                  harga_barang:$('#harga').val(),
                  qty:$('#stok2').val(),
              },
        cache: false,
        dataType: "text",
        success: function(data){
           $('#body-table').html(data);  

           $.ajax({
              type: "POST",
              url: "<?php echo base_url() ?>transaksi/totalcart",
              dataType: "text",
              success:function(data){
                $('#total').val(data); 
                 showKembali($('#bayar').val());

            }
          });

        $('.reset').val('');
        }

      });
    }
    
  });


  function convertToRupiah(angka)
  {

      var rupiah = '';    
      var angkarev = angka.toString().split('').reverse().join('');
      
      for(var i = 0; i < angkarev.length; i++) 
        if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
      
      return rupiah.split('',rupiah.length-1).reverse().join('');
  
  }

  function subTotal(stok2)
  {

    var harga = $('#harga').val().replace(".", "").replace(".", "");

    $('#subtotal').val(convertToRupiah(harga*stok2));
  }

  function deletebarang(id,subtotal)
    {
        // ajax delete data to database
          $.ajax({
            url : "<?= site_url('transaksi/deletebarang')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
              
               refresh_tabel();

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

          var ttl = $('#total').val().replace(".", "");

          $('#total').val(convertToRupiah(ttl-sub_total));

          showKembali($('#bayar').val());
          
    }

    function showKembali(str)
    {
      var total = $('#total').val().replace(".", "").replace(".", "");
      var bayar = str.replace(".", "").replace(".", "");
      var kembali = bayar-total;

      $('#kembali').val(convertToRupiah(kembali));



      if (kembali >= 0) {
        $('#selesai').removeAttr("disabled");
      }else{
        $('#selesai').attr("disabled","disabled");
      };

      if(total == '0'){
        $('#batal').attr("disabled","disabled");
      }
      else{
        $('#batal').removeAttr("disabled");
         
      }

      if (total == '0') {
        $('#selesai').attr("disabled","disabled");
      };
    }

  function refresh_tabel(){
    $.ajax({
        type: "POST",
        url: "<?php echo base_url() ?>transaksi/list_transaksi", // ke script controller
        cache: false,
        dataType: "text",
        success: function(data){
           $('#body-table').html(data);
           
        },

        
      });
     $.ajax({
      type: "POST",
      url: "<?php echo base_url() ?>transaksi/totalcart",
      dataType: "text",
      success:function(data){
        $('#total').val(data);
         showKembali($('#bayar').val());
      }
    })
    

  }
    
    function get_detail(barangs){
      $.ajax({
        type:'post',
          url: '<?php echo base_url() ?>transaksi/get_data_monitoring',

          data: {
            'barangs': barangs
          },
          success:function(response){
            var data = response.split('|');
            var nama_barang = data[0];
            var satuan = data[1];
            var harga = data[2];
            var stok = data[3];
            $('#stok2').val('');
            $('#stok2').attr('disabled', false);

            if(stok == 0) $('#stok2').attr("disabled", true); 

            $('#nama_barang').val(nama_barang);
            $('#satuan').val(satuan);
            $('#harga').val(harga);
            $('#stok').val(stok);
          }

          
      });
    }
  
    
    $('input#harga').keyup(function(event) 

    {

  // skip for arrow keys
  if(event.which >= 37 && event.which <= 40) return;

  // format number
  $(this).val(function(index, value) {
    return value
    .replace(/\D/g, "")
    .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
    ;
  });
});

    $('input#bayar').keyup(function(event) 

    {

  // skip for arrow keys
  if(event.which >= 37 && event.which <= 40) return;

  // format number
  $(this).val(function(index, value) {
    return value
    .replace(/\D/g, "")
    .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
    ;
  });
});



  </script>



  

  