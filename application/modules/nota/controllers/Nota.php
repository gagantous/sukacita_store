<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nota extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_nota_stok');
		$this->load->model('M_nota');
		$this->load->helper('url');
		$this->load->library('cart');
		$ses = $this->session->userdata('sukacita');
		if(!$ses){
			redirect(base_url('login'));
		}
		/* if($ses['role'] != 'super'){
			redirect(base_url().'error');
		} */

	}

	public function index(){
		$sess = $this->session->userdata('sukacita');
		$data['nama'] = $sess['nama'];
		$data['nama_role'] = $sess['nama_role'];
		$data['id_admin'] = $sess['id_admin'];
		

		$data['barang'] = $this->M_nota_stok->get_nota()->result();
		//$data['nomor'] = 'T'.date('ym').str_pad($this->M_nota->get_no() + 1, 3, '0', STR_PAD_LEFT);
		 
		
		//$data['gol'] = $this->M_siswa->get_siswa()->result();
		$this->load->view('header',$data);
         $this->load->view('sidebar');
		$this->load->view('V_nota_index', $data);
		$this->load->view('footer');

	}

	 public function selesai(){
	 	$sess = $this->session->userdata('sukacita');
		 $data['id_admin'] = $sess['id_admin'];
		 $id_admin = $sess['id_admin'];

		 $submit = $this->input->post('submit');

		 if($submit){


		 	$nomornotanya = $this->input->post('nomornotanya');
		 	$bayar = $this->input->post('bayar');
		 	$kembali = $this->input->post('kembali');

		 	$order = array(
					'no_nota' => $nomornotanya,
					'id_admin' => $id_admin,
					'bayar' => str_replace(".","",$bayar),
					'kembali' => str_replace(".","",$kembali)
					);

		 	$this->M_nota->order($order);

		 	$dapat = $this->M_nota->lastdata();
		 	$idakhir = $dapat[0]['id'];


	 	if ($cart = $this->cart->contents()):
			foreach ($cart as $item):



				$order_detail = array(
				'id_nota' => $idakhir,
				'id_barang' => $item['id'],
				'qty' => $item['qty'],
				'subtotal' => $item['subtotal'],
				//'bayar' => str_replace("."," ",$bayar)
				);	


				$brg = $this->db->get_where('barang', array('id' => $item['id']))->row();
			
				$brg1 = array (
					'stok'	=> $brg->stok - $item['qty']
				);

				$status = array(
					'id_barang' => $item['id'],
					'sejumlah' => $item['qty'],
					'status' => "nota"
					);
				$stat = $this->M_nota->status($status);

				$this->db->update('barang', $brg1, array('id' => $item['id']));
				$cust_id = $this->M_nota->order_detail($order_detail);
				
			endforeach;
		endif;
		}
		$this->cart->destroy();

		redirect( base_url('nota'));
	}
		/*
		$nomor_nota = $this->input->post('nota');
		$id_barangs = $this->input->post('id_barangs');
		$explode = explode(',', $id_barangs);
		for ($i=0; $i < ; $i++) { 
			# code...
		}
		
		} 
	
		$sess = $this->session->userdata('sukacita');
		$id_admin = $data['id_admin'];

		foreach ($this->cart->contents() as $key) {

			$data_barang = array(
				'no_nota' => $nota,
				'id_barang' => $key['id'],
				'id_admin' => $id_admin

			);
		 	
		 	 $this->M_nota->order($data_barang);
		 	 print_r('berhasil');

		} 
	}

		*/
	public function batal(){
		$this->cart->destroy();

		redirect( base_url('nota'));
	}
	
	public function list_nota(){

		$table=null;$no=1;$st=0;
			$q = $this->cart->contents();
			foreach ($q as $row) {
				$st= 'Rp. ' . number_format(($row["qty"]*$row["price"]), 
                    0 , '' , '.' );
				
				$hapus = '<a 
				href="javascript:void()" style="color:rgb(255,128,128);
				text-decoration:none" onclick="deletebarang('
					."'".$row["rowid"]."'".','."'".$row['subtotal'].
					"'".')"> <i class="fa fa-close"></i> Delete</a>';

				$table.="<tr><td>{$no}</td><td>{$row['id']}</td>
                  <td>{$row['name']}</td>
                  <td>{$row['price']}</td>
                  <td>{$row['qty']}</td>
                  <td>{$st}</td>
                  <td>{$hapus}</td>
            	</tr>";
            	$no++;
			}
		
		echo $table;

	}

	public function jumlahsemua(){

		$jumlahsemua = $this->cart->total();
		echo $jumlahsemua;
	}

	public function addbarang()
	{

		/*
		$data = array(
				'name' => $this->input->post('nama_barang'),
				'sat' => $this->input->post('satuan'),
				'id' => $this->input->post('id_barang'),
				'price' => str_replace('.', '', $this->input->post(
					'harga_barang')),
				'qty' => $this->input->post('qty')
			);
			*/
		$hrg=str_replace('.', '', $this->input->post('harga_barang'));
		$data = array(
		    'id'      => $this->input->post('id_barang'),
		    'qty'     => $this->input->post('qty'),
		    'price'   => $hrg,
		    'name'    => $this->input->post('nama_barang'),
		    'options' => array('satuan' => $this->input->post('satuan'))
		);
		$insert = $this->cart->insert($data);
		$table=null;
		if($insert){
			// Get item dalam cart
			$table=null;$no=1;$st=0;
			$q = $this->cart->contents();
			foreach ($q as $row) {
				$st= 'Rp. ' . number_format(($row["qty"]*$row["price"]), 
                    0 , '' , '.' );

				$hrg = 'Rp. '.number_format($row['price'],0,',','.');
				
				

				$hapus = '<a 
				href="javascript:void()" style="color:rgb(255,128,128);
				text-decoration:none" onclick="deletebarang('
					."'".$row["rowid"]."'".','."'".$row['subtotal'].
					"'".')"> <i class="fa fa-close"></i> Delete</a>';

				$table.="<tr><td>{$no}</td><td>{$row['id']}</td>
                  <td>{$row['name']}</td>
                  <td>{$hrg}</td>
                  <td>{$row['qty']}</td>
                  <td>{$st}</td>
                  <td>{$hapus}</td>
            	</tr>";
            	$no++;
			}
		}
		echo $table;
	}

	function coba(){
		$q = $this->cart->contents();
		print_r($q);

	}

	function coba2(){
		$q = $this->cart->destroy();

	}

	function totalcart(){
		$q = number_format($this->cart->total(),0,',','.');
		echo $q;
	}



	public function deletebarang($rowid) 
	{

		$delete = $this->cart->update(array(
				'rowid'=>$rowid,
				'qty'=>0,));
		echo $delete;
	}

	function get_data_monitoring(){
		$barangs = $this->input->post('barangs');
		$cek = $this->M_nota->get_id_barang($barangs);
		$nama_barang = "";
		$satuan = "";
		$harga = "";
		$stok = "";

		foreach ($cek as $monitor) {
			$nama_barang = $monitor->nama_barang;
			$satuan = $monitor->satuan;
			$harga = number_format($monitor->harga,0,',','.');
			$stok = $monitor->stok;

	
		}

		$data_monitoring = $nama_barang."|".$satuan."|".$harga."|".$stok;
		
		echo $data_monitoring;
	}

}
/*
	public function aaa(){

		echo $this->input->post('idbarang');
	}

	
}

public function aaa(){
		$id_barang = $this->input->post('idbarang');
		$this->db->where('id_barang', $idbarang);
		$query = $this->db->get("tabel");
		$num_rows = $query->num_rows();
		$a = '';
		if($num_rows > 0){
			$result = $query->result();

			foreach ($$result as $key) {
				$a .= 'nama : '.$key->nama.' ';
				$a .= 'nim : '.$key->nim.'</br>';
			}
		}else{
			$a = 'tidak ada data';
		}
		echo $a;
	}



public function getbarang($id)
	{

		$barang = $this->M_nota->get_by_id($id);

		if ($barang) {

			if ($barang->stok == 0) {
				$disabled = 'disabled';
				$info_stok = '<span class="help-block badge" id="reset" 
					          style="background-color: #d9534f;">
					          stok habis</span>';
			}else{
				$disabled = '';
				$info_stok = '<span class="help-block badge" id="reset" 
					          style="background-color: #5cb85c;">stok : '
					          .$barang->stok.'</span>';
			}

			echo '<div class="form-group">
                  <label for="satuan" class="col-sm-3 control-label">SATUAN</label>

                  <div class="col-sm-4">
                    <input type="text" name="satuan" id="satuan" DISABLED class="form-control input-lg"  placeholder="Satuan Barang" value="'.$barang->satuan.'">
                  </div>
                </div>
                <div class="form-group">
                  <label for="harga" class="col-sm-3 control-label">HARGA (RP)</label>

                  <div class="col-sm-6" >
                    <input type="" id="harga_barang" name="harga_barang" DISABLED class="form-control input-lg" placeholder="Harga barang" value="'.number_format( $barang->harga, 0 ,
				        	 '' , '.' ).'">
                  </div>
                </div>
                <div class="form-group">
                  <label for="stok" class="col-sm-3 control-label">QUANTITY</label>

                  <div class="col-sm-6">
                   
				        <input type="number" class="form-control input-lg reset" 
				        	name="qty" placeholder="Jumlah barang" autocomplete="off" 
				        	id="qty" onchange="subTotal(this.value)" 
				        	onkeyup="subTotal(this.value)" min="0"  
				        	max="'.$barang->stok.'" '.$disabled.'>
				      </div>'.$info_stok.'</div>
                </div>';
	    }else{

	    	echo '<div class="form-group">
                  <label for="satuan" class="col-sm-3 control-label">SATUAN</label>

                  <div class="col-sm-4">
                    <input type="text" name="satuan" id="satuan" DISABLED class="form-control input-lg"  placeholder="Satuan Barang">
                  </div>
                </div>
                <div class="form-group">
                  <label for="harga" class="col-sm-3 control-label">HARGA (RP)</label>

                  <div class="col-sm-6" >
                    <input type="" id="harga_barang" name="harga_barang" DISABLED class="form-control input-lg" placeholder="Harga barang" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="stok" class="col-sm-3 control-label">QUANTITY</label>

                  <div class="col-sm-6">
                  
				        <input type="number" class="form-control input-lg reset" 
				        	name="qty" placeholder="Jumlah Barang" autocomplete="off" 
				        	id="qty" onchange="subTotal(this.value)" 
				        	onkeyup="subTotal(this.value)" min="0"  
                </div>';
	    }

	}
	*/

