<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_login');
		$ses = $this->session->userdata('sukacita');
		if($ses){
			redirect(base_url('dashboard'));
		}
	}

	public function index()
	{
		

		$submit = $this->input->post('submit');

		if($submit){
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$login = $this->M_login->cek_login($username, md5($password));

			if($login){
				foreach($login as $admin){
					$id_admin = $admin->id_admin;
					$nama = $admin->nama;
					$role = $admin->nama_role;
				}
				$sess_array = array(
					'id_admin' => $id_admin,
					'user' => $username,
					'nama' => $nama,
					'nama_role' => $role
					); 

				$this->session->set_userdata('sukacita', $sess_array);
				redirect('dashboard');
			
			}
				
			else{
				$msg['error'] = true;
				$this->load->view('V_login', $msg);
			}
		}
		else{
			$this->load->view('V_login');
		}
	}
	
}

/*

function __construct(){
		parent::__construct();
		$this->load->model('M_login','login',TRUE);

	}

	public function index(){

		$us = $this->sessin->userdata('login');
		$data['act'] = 'log'
		$log['pesan'] = "Username atau password salah";

		if ($us == TRUE) {
			redirect('dashboard');
		}
		else{
			if($this->login->validasi()){
				if($this->login->cek_user()){
					redirect('dashboard');
				}
				else{
					$this->load->view('login', $log);
				}
			}
			else{
				$this->load->view('login',$log)
			}
		}
	
	}

	public function logout(){
		$this->login->logout;
		redirect('')
	}
	

*/