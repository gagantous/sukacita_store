<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class M_login extends CI_Model {


	function cek_login($username = null, $password = null){
		$this->db->select('*');
		$this->db->from('admin a');
		$this->db->join('role r','r.id = a.id_role');
		$this->db->where('a.user', $username);
		$this->db->where('a.password', $password);

		$query = $this->db->get();
		return $query->result();
	}

	function cek_transaksi(){
		$tgl = date('Y-m-d');
		$nul = '0';
		$this->db->select('bayar,tgl_transaksi');
		$this->db->from('transaksi');
		$this->db->where('bayar',$nul);
		$this->db->where('date(tgl_transaksi)',$tgl);

		$query = $this->db->get();
		return $query->result(); 

	}

	function tambah_transaksi($data){
		$ber = $this->db->insert('transaksi', $data);
		return $ber;
	}

	function order($data){
		$ber = $this->db->insert('transaksi', $data);
		return $ber;

	}


}




/*
--- 
ini adalah contoh model yang tidak digunakan

public function load_form_rules(){
		$form_rules = array(
						array(
							'field' => 'username',
							'label' => 'Username',
							'rules' => 'required'),
						array(
							'field' => 'password',
							'label' => 'Password',
							'rules' => 'required'),
						);
		return $form_rules;
	}
	public function validasi(){
		$form = $this->load_form_rules();
		$this->form_validation->set_rules($form);

		if($this->form_validation->run)(
			return TRUE;)
		else(
			return FALSE;)
	}

public function cek_user(){
	$username = $this->input->post('username');
	$password = md5($this->input->post('password'));

	$query = $this->db->where('username',$username)
	->where('password', $password);
	->limit(1);
	->get($this->$tabel);
	if($query->num_rows() == 1)(
		$data = array('username' => $username), 'login' => TRUE;
		$this->session->set_userdata($data);
		
		return TRUE;
	)
		else(
			return FALSE;
			)
}
	public function get_login(){
		return $this->db->get($tabel);
	}

*/