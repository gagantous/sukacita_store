<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {


	function __construct(){
		parent::__construct();
		$this->load->model('M_user');
		
		$ses = $this->session->userdata('sukacita');
		if(!$ses){
			redirect(base_url('login'));
		}

		if($ses['nama_role'] == 'kasir' ){
			redirect(base_url().'error');
		}
	}

	public function index(){
		$sess = $this->session->userdata('sukacita');
		$data['nama'] = $sess['nama'];
		$data['nama_role'] = $sess['nama_role'];
		$data['aktif'] = "user";
		$data['nama_modul'] = "MANAJEMEN USER";
		$data['title'] = "MANAJEMEN USER";
		$data['gol'] = $this->M_user->get_user()->result();
		//$data['gol'] = $this->M_siswa->get_siswa()->result();
		$this->load->view('header',$data);
         $this->load->view('sidebar');
		$this->load->view('V_index_user',$data);
		$this->load->view('footer');

	}

	public function tambah(){
		
		$sess = $this->session->userdata('sukacita');
		$data['nama'] = $sess['nama'];
		$data['nama_role'] = $sess['nama_role'];
		$data['rl'] = $this->M_user->get_role()->result();
		$data['aktif'] = "user";
		$data['nama_modul'] = "tambah user";
		$data['title'] = "TAMBAH USER";

			$this->load->view('header',$data);
         	$this->load->view('sidebar');
			$this->load->view('V_tambah_user',$data);
			$this->load->view('footer');
		}

	public function tambah_b(){
		$sess = $this->session->userdata('sukacita');
		$data['nama'] = $sess['nama'];
		$data['nama_role'] = $sess['nama_role'];
		$data['act'] = 'TAMBAH USER';
		$submit = $this->input->post('submit');
		
		if($submit){

			$nama = $this->input->post('nama');
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$id_role = $this->input->post('id_role');
			$email = $this->input->post('email');
			$nohp = $this->input->post('nohp');
			$alamat = $this->input->post('email');
							
				$user_arr = array(
						
						'nama' => $nama,
						'user' => $username,
						'password' => md5($password),
						'id_role' => $id_role,
						'email' => $email,
						'nohp' => $nohp,
						'alamat' => $alamat
						//'id' => ''
				);
			$check = $this->M_user->check($username);
			if($check <= 0){
				$tambah = $this->M_user->input_user($user_arr);
				if(!$tambah){
						$this->session->set_flashdata('alert','
							<div class="alert alert-success alert-dismissible">
		                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                <h4><i class="icon fa fa-check"></i> Sukses!</h4>
		                Sukses Menambahkan data!
		              </div>
							');
					}else{
						$this->session->set_flashdata('alert','
							<div class="alert alert-danger alert-dismissible">
		                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                <h4><i class="icon fa fa-ban"></i> Peringatan!</h4>
		                Gagal Menambahkan data dikarenakan terdapat masalah pada jaringan atau sistem silahkan ulangi lagi proses penginputan, hubungi pihak administrator!
		              </div>
							');
					}
						redirect(base_url('user'));
					}else{
					$this->session->set_flashdata('alert','
								<div class="alert alert-warning alert-dismissible">
			                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			                <h4><i class="icon fa fa-ban"></i> Peringatan!</h4>
			                Username <b class="bg-red">'.$username.'</b> sudah terdaftar, silahkan masukkan username lain
		              </div>
							');
						redirect(base_url('user/tambah'));
			}

	}
}

public function hapus_user($id){
		
			$del = $this->M_user->hapus($id);
			if($this->db->affected_rows()){
				$this->session->set_flashdata('alert','
					<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                <h4 style="font-weight: bold;"><i class="icon fa fa-check"></i> SUKSES MENGHAPUS DATA USER!</h4>
                BERHASIL MENGHAPUS DATA!
              </div>
					');
			}else{
				$this->session->set_flashdata('alert','
					<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                <h4><i class="icon fa fa-ban"></i> GAGAL MENGHAPUS DATA USER!</h4>
                Gagal Menghapus data dikarenakan terdapat masalah pada jaringan atau sistem. </br> Silahkan ulangi lagi proses penginputan/hubungi pihak administrator!
              </div>
					');
			}
			redirect(base_url('user'));
		
	}

public function ubah($id = null)
	{
		$sess = $this->session->userdata('sukacita');
		$data['nama'] = $sess['nama'];
		$data['nama_role'] = $sess['nama_role'];
		
		$data['act'] = 'user';
		
		$submit = $this->input->post('submit');

		if($submit){

			$xid['id_admin'] = $this->input->post('id');
			$nama = $this->input->post('nama');
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$id_role = $this->input->post('id_role');
			$email = $this->input->post('email');
			//$id_admin = $this->input->post('id_admin');

			if($password){		
				$user_arr = array(
						
						'nama' => $nama,
						// 'user' => $username,
						'password' => md5($password),
						'id_role' => $id_role,
						'email' => $email,
						'nohp' => $nohp,
						'alamat' => $alamat
				);
			}else{
				$user_arr = array(
						
						'nama' => $nama,
						// 'user' => $username,
						'id_role' => $id_role,
						'email' => $email,
						'nohp' => $nohp,
						'alamat' => $alamat
						//'id_admin' => $id_admin
				);

			}
		
			$tambah = $this->M_user->update_user($user_arr, $xid);
			if(!$tambah){
				$this->session->set_flashdata('alert','
					<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Peringatan!</h4>
                Gagal Mengubah data dikarenakan terdapat masalah pada jaringan atau sistem silahkan ulangi lagi proses penginputan, hubungi pihak administrator!
              </div>
					');
			}else{
				$this->session->set_flashdata('alert','
					<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Sukses!</h4>
                Sukses Mengubah data!
              </div>
					');
			}
			redirect('user');
			
		}
		else{

			$cek = $this->M_user->cek_user_id($id);
			if($cek){
				$user['data'] = $this->M_user->get_user_id($id);
				$user['rl'] = $this->M_user->get_role()->result();
				$data['aktif'] = "user";
				$data['menu_modul'] = "UBAH USER";
				$data['title'] = "EDIT USER";
				$this->load->view('header',$data);
         		$this->load->view('sidebar');
				$this->load->view('V_edit_user', $user);
				$this->load->view('footer');
			}else{
				redirect(base_url().'error');
			}
			
		}
	}

	
	
function cetak($id){
	$this->load->model('M_riwayat_transaksi');
	$this->load->library('printers');
	$data2 = $this->M_riwayat_transaksi->riwayat($id);
	$data = $this->M_riwayat_transaksi->get_riwayat_transaksi($id);
	$this->printers->connect("POS-58");
	$this->printers->print_test_receipt($data,$data2);
}

}
