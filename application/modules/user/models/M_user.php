<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model {
	
	public $tabel = 'admin';


	public function get_user()
	{
		$this->db->select('*');
		$this->db->from('admin a');
		$this->db->join('role r','r.id = a.id_role');
		
		$query = $this->db->get();
		return $query;
	}

	public function get_role(){
		return $this->db->get('role');

	}

	public function input_user($data){
		$this->db->insert($this->tabel, $data);

	}

	 public function cek_user_id($id){
    	$this->db->where('id_admin', $id);
    	$query = $this->db->get($this->tabel);

    	return $query->num_rows();
    }

    public function check($user){
    	$this->db->where('user', $user);
    	$query = $this->db->get($this->tabel);

    	return $query->num_rows();
    }

   
    public function get_user_id($id){
		$this->db->where('id_admin', $id);
		$query = $this->db->get($this->tabel);

		return $query->result();
	}

	 public function hapus($id){
    	$this->db->where('id_admin', $id);
    	$this->db->delete($this->tabel);
    }


	public function update_user($data, $id)
    {
        $query = $this->db->update($this->tabel, $data, $id);
        return $query;
    }



	public function edit_user($data, $table){
		$res = $this->db->get_where($table, $where);
		return $res;
	}

	public function cek_role(){
		$this->db->select('*');
		$this->db->from('role');
		$query = $this->db->get();

		return $query;
	}

}
