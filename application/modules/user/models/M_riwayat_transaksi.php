<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_riwayat_transaksi extends CI_Model {
	
	public $tabel = 'detail_transaksi';
	/*
	public function get_riwayat_transaksi()
	{
		$this->db->select('a.no_nota,t.*,b.nama_barang');
		$this->db->from('detail_transaksi t');
		$this->db->join('transaksi a','a.id = t.id_nota');
		$this->db->join('barang b','b.id = t.id_barang');
		$query = $this->db->get();
		return $query;
	}
	*/

	public function riwayat($id){
		$this->db->select('*');
		$this->db->from('transaksi');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_riwayat_transaksi($id)
	{

		$query = $this->db->query('select a.id,b.id_barang,e.nama_barang, b.qty, b.subtotal, b.barang_m, b.harga_m  from transaksi a left outer join detail_transaksi b on a.id = b.id_nota left outer join barang e on e.id = b.id_barang where a.id='.$id);
		
		$re = $query->result();
		return $re;
	}
	

	public function get_riwayat(){
		$this->db->select('t.*,a.user');
		$this->db->from('transaksi t');
		$this->db->join('admin a','a.id_admin = t.id_admin');
		$this->db->order_by('no_nota','desc');
		
		$query = $this->db->get();
		return $query;
	}

	public function input_riwayat_transaksi($data){
		$this->db->insert($this->tabel, $data);

	}

	public function update_riwayat_transaksi($data, $id)
    {
        $query = $this->db->update($this->tabel, $data, $id);
    }


	public function edit_riwayat_transaksi($data, $table){
		$res = $this->db->get_where($table, $where);
		return $res;
	}

	function get_last_id(){
		$this->db->order_by("id","desc");
		$this->db->select('*');
		$this->db->from('barang');
		$query=$this->db->get();
		return $query->result();
	}

	function status($data){
		$ber = $this->db->insert('status', $data);
		return $ber;

	}

	function cek_riwayat_transaksi_id($id){
    	$this->db->where('id', $id);
    	$query = $this->db->get($this->tabel);

    	if ($query->num_rows() > 0)
        {return $query->row_array();}
        else {return NULL;}
    }

    function get_riwayat_transaksi_id($id){
		$this->db->where('id', $id);
		$query = $this->db->get($this->tabel);

		return $query->result();
	}

	function hapus($id){
    	$this->db->where('id', $id);
    	$this->db->delete('transaksi');
    	
    }

    function hapus_semua() {
	
			$delete = $this->input->post('msg');
			for ($i=0; $i < count($delete) ; $i++) { 
				$this->db->where('id', $delete[$i]);
				$this->db->delete('transaksi');
			}
	}
}
