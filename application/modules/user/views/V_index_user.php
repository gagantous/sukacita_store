
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo strtoupper(@$nama_modul) ?>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo $this->session->flashdata('alert'); ?>
      <div class="row">
      <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <a type="button" class="pull-right btn btn-primary" role="button" href="<?php echo base_url(); ?>user/tambah"><span class="fa fa-plus"></span> TAMBAH</a>
            </div>
            <div class="box-body">
              <table id="example1"  class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th style="padding: 5px;">No</th>
                  <th>Nama</th>
                  <th>User Name</th>                  
                  <th>Role</th>
                  <th>Email</th>
                  <th>Aksi</th>
                  <!-- <th>Harga Barang</th>
                  <th>Stock Barang</th> -->
                </tr>
                </thead>
                <tbody>
                 <?php $no = 1;
                  foreach ($gol as $user) { 
                ?>
                <tr>
                   <td><?php echo $no++; ?></td >
                  <td><?php echo ucwords($user->nama); ?></td>
                  <td><?php echo $user->user; ?></td>
                  <td><?php echo $user->nama_role; ?></td>
                  <td><?php echo $user->email; ?></td>
                  <td width="100px">
                  <div class="btn-group">
                  <?php if ($user->nama_role != 'super') { ?>
                  <a name="submit" id="submit" class="btn btn-info" role="button" href="<?php echo base_url() ?>user/ubah/<?php echo $user->id_admin?>" alternative="Edit barang"><i class="glyphicon glyphicon-edit" style=""></i></a> 
                  <button type="button" class="hap pull-left btn btn-danger" data-toggle="modal" data-id="<?php echo $user->id_admin ?>"  data-target="#myModaluser" role="button"><i class="glyphicon glyphicon-trash"></i></button>
                  <?php } ?>
                  </div>
                  </td>
                </tr>
                <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        <!-- /.col -->
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <div class="modal fade modal-danger " id="myModaluser" role="dialog" >
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <center><h3 class="modal-title"><i class="glyphicon glyphicon-exclamation-sign"style="font-size: 20px;"></i> PERINGATAN</h3></center> 
                  </div>
                  <div class="modal-body">
                    <h3><center>Apakah anda yakin ingin menghapus ? <br/></h3> </center>
                  </div>
                  <div class="modal-footer">
                    <button type="button" style="font-size: 20px;" class="btn btn-outline pull-left" data-dismiss="modal">Kembali</button>
                    <button type="button" style="font-size: 20px;" onclick="hapususer()" data-hapus="" id="hapusmodal" class="btn btn-outline"><i class="glyphicon glyphicon-remove" style="font-size: 20px;"></i> Hapus </button>
                  </div>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
  <!-- /.content-wrapper -->

   <script type="text/javascript">

     $(function () {
          $('.hap').click(function(){
          //get cover id
          var id = $(this).data('id');
          //set href for cancel button
          $('#hapusmodal').data('hapus',id);
          })

              $("#example1").DataTable({
      "aoColumnDefs": [
           {

               "bSortable": false,
               "aTargets": ["tutup"]
           }
        ],
        "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "Semua"] // change per page values here
              ],
        "language": {
                "lengthMenu": "Tampilkan _MENU_ data",
                "paginate": {
                  "previous":"Prev",
                  "next": "Next",
                  "last": "Last",
                  "first": "First"
                }
              },


    });
      });
     
       function hapususer(){
          var id = $("#hapusmodal").data("hapus");
          location.href = '<?php echo base_url(); ?>user/hapus_user/'+id;
        }


    </script>
 