  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo strtoupper(@$nama_modul); ?>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo $this->session->flashdata('alert'); ?>
      <div class="row">

      <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-danger">
            <div class="box-header with-border">
        			<div>
                      <h3 class="box-title"><?php echo ucwords(@$nama_modul) ?></h3>
        			</div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="<?php echo base_url().'user/tambah_b'?>" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                
                <div class="form-group">
                  <label for="Text" class="col-sm-4 control-label">Nama Pengguna <span class="text-red">*</span></label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" required="">
                  </div>
                </div>
               <div class="form-group">
                        <label for="Text" class="col-sm-4 control-label">UserName <span class="text-red">*</span></label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="username" name="username" placeholder="User Name" required="">
                        </div>
                      </div>
      				<div class="form-group">
                  <label for="Text" class="col-sm-4 control-label">Password <span class="text-red">*</span></label>
                      <div class="col-sm-6">
                          <input type="password" class="form-control" id="password" name="password" placeholder="Password" required="">
                     </div>
              </div>
      				<div class="form-group">
                  <label class="col-sm-4 control-label">Hak Akses</label>
                    <div class="col-sm-6">
                        <select name="id_role" class="form-control" required="">
                        <?php foreach ($rl as $rl) { ?>
                        <option value="<?php echo $rl->id ?>"><?php echo $rl->nama_role ?></option>
                        <?php } ?>
                        </select>
                   </div>
                </div>
      				    <div class="form-group">
                        <label for="Text" class="col-sm-4 control-label">Email</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                        </div>
                      </div>
                     <div class="form-group">
                        <label for="nohp" class="col-sm-4 control-label">No. HP</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" id="nohp" name="nohp" placeholder="No. Hp">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="Text" class="col-sm-4 control-label">Alamat </label>
                        <div class="col-sm-6">
                          <textarea class="form-control" id="alamat" name="alamat" cols="4" rows="5" ></textarea>
                        </div>
                      </div>

                </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="<?php echo base_url() ?>user" class="btn btn-danger pull-left">Kembali</a>
               <!-- <button type=""  class="btn btn-default">Batal</button>  -->
                <input type="submit" id="submit" name="submit" class="btn btn-info pull-right" value="Tambah" > 
              <!-- /.box-footer -->
              </div>
              </form>
            </div>
          </div>
        </div>
      </section>
  </div>
  <!-- /.content-wrapper -->
  