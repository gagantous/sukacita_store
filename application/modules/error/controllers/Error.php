<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$ses = $this->session->userdata('sukacita');
		if(!$ses){
			redirect(base_url().'login');
		}
	}

	public function index()
	{
		//notifikasi
		
		$sess = $this->session->userdata('sukacita');
		$data['nama'] = $sess['nama'];
		$data['nama_role'] = $sess['nama_role'];
		$data['act'] = '';

		$this->load->view('header', $data);
		$this->load->view('V_error');
		$this->load->view('footer');
	}
}
