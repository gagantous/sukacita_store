<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        404 Error Halaman
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>dashboard"><i class="fa fa-dashboard"></i>DASHBOARD</a></li>
        
        <li class="active">404 error</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="error-page">
        <h2 class="headline text-yellow"> 404</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-yellow"></i> Halaman tidak ditemukan </h3>

          <p>
            Kami tidak dapat menemukan halaman yang anda cari.</br> Halaman yang anda mungkin rusak atau anda tidak diberi hak akses.
            Silahkan Kembali ke<a href="<?php echo base_url() ?>dashboard"> Dashboard</a> 
          </p>

          
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>
    <!-- /.content -->
  </div>