<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_rbarang extends CI_Model {
	
	public $tabel = 'status';


	public function get_rbarang()
	{
		$this->db->select('s.id_barang , b.nama_barang , s.id , s.sejumlah , s.tanggal_diubah, s.status');
		$this->db->from('status s');
		$this->db->join('barang b','b.id = s.id_barang' );
		$this->db->order_by('tanggal_diubah', 'DESC');
		
		$query = $this->db->get();
		return $query;
	}





	public function input_rbarang($data){
		$this->db->insert($this->tabel, $data);

	}

	public function update_rbarang($data, $id)
    {
        $query = $this->db->update($this->tabel, $data, $id);
    }


	public function edit_rbarang($data, $table){
		$res = $this->db->get_where($table, $where);
		return $res;
	}

	function get_last_id(){
		$this->db->order_by("id","desc");
		$this->db->select('*');
		$this->db->from('barang');
		$query=$this->db->get();
		return $query->result();
	}

	function status($data){
		$ber = $this->db->insert('status', $data);
		return $ber;

	}

	 public function cek_rbarang_id($id){
    	$this->db->where('id', $id);
    	$query = $this->db->get($this->tabel);

    	if ($query->num_rows() > 0)
        { return $query->row_array();
        }
        else {return NULL;}
    }

    public function get_rbarang_id($id){
		$this->db->where('id', $id);
		$query = $this->db->get($this->tabel);

		return $query->result();
	}

	function hapus($id){
    	$this->db->where('id', $id);
    	$this->db->delete('status');
    	
    }

    function hapus_semua() {
			$delete = $this->input->post('msg');
			for ($i=0; $i < count($delete) ; $i++) { 
				$this->db->where('id', $delete[$i]);
				$this->db->delete('status');
			}
	
    }
}
