<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rbarang extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_rbarang');
		$this->load->helper('url');
		$ses = $this->session->userdata('sukacita');
		if(!$ses){
			redirect(base_url('login'));
		}
		if($ses['nama_role'] == 'admin'){
			redirect(base_url().'error');
		}
	}

	public function index(){
		$sess = $this->session->userdata('sukacita');
		$data['nama'] = $sess['nama'];
		$data['nama_role'] = $sess['nama_role'];
		
		$data['gol'] = $this->M_rbarang->get_rbarang()->result();
		$this->load->view('header',$data);
         $this->load->view('sidebar');
		$this->load->view('V_index_rbarang',$data);
		$this->load->view('footer');

	}

	public function hapus($id){
		$this->M_rbarang->hapus($id);
		redirect(base_url('rbarang'));
	}

	public function  hapus_banyak() {
			$this->M_rbarang->hapus_semua();
			redirect('rbarang');
			
		}


	
}
