
    <!-- Main content -->
<div class="content-wrapper">
     <section class="content-header">
      <h1>
        <?php echo strtoupper(@$nama_modul) ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">RIWAYAT BARANG</li>
      </ol>
    </section>
    <section class="content">
    <div class="col-md-12">
      <div class="row">
          <div class="box box-info">
              <div class="box-header">
                 <a type="button" class="pull-LEFT btn btn-md btn-danger" data-toggle="modal" data-target="#myModalstok1" role="button">Hapus</a>
              </div>
            <!-- /.box-header -->
            <div class="box-body">
            <form class="form-horizontal" action="<?php echo base_url() ?>rbarang/hapus_banyak" method="POST" >
              <table id="example1"  class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th class="tutup"><input type="checkbox" id="select_all" /></th>
                  <th>No</th>
                  <th>ID_barang</th>
                  <th>Nama barang</th>
                  <th>Sejumlah</th>
                  <th>Tanggal perubahan</th>
                  <th>Keterangan</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
               
                <?php 
                $no=1;
                foreach ($gol as $key ) { 
               
                  ?>
                  <tr>
                  <td><input type="checkbox" name="msg[]" value="<?php echo $key->id; ?>"/></td>
                  <td><?php echo $no++ ?></td>
                  <td><?php echo $key->id_barang ?></td>
                  <td><?php echo $key->nama_barang?></td>
                  <td><?php echo $key->sejumlah ?></td>
                  <td><?php $month_num = date('d F Y', strtotime($key->tanggal_diubah));
                      echo $month_num;  ?></td>
                  <td><?php echo $key->status ?></td>
                  <td>
                  <a href="#myModalstok" type="button" data-id="<?php echo $key->id ?>"  data-toggle="modal" class="hap btn btn-md btn-danger"><i class="glyphicon glyphicon-trash"></i></a></td>
                </tr>

                  <?php }?>
				  
                
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  </div>
  <div class="modal fade modal-danger " id="myModalstok1" role="dialog" >
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <center><h3 class="modal-title"><i class="glyphicon glyphicon-exclamation-sign"style="font-size: 20px;"></i> PERINGATAN</h3></center> 
                  </div>
                  <div class="modal-body">
                    <h3><center>Anda akan menghapus data yang sudah anda pilih</br> Apakah anda yakin?<br/></h3><br/> </center>
                  </div>
                  <div class="modal-footer ">
                  <div class="text-center ">
                    <input type="submit" style="font-size: 20px; width:60px" name="submit"  value="YA" class="btn btn-outline">
                    <button type="button" style="font-size: 20px;" class="btn btn-outline pull-center" data-dismiss="modal">TIDAK</button>
                    <!-- <button type="button" style="font-size: 20px;" onclick="hapusstoksemua()" class="btn btn-outline"><i class="glyphicon glyphicon-remove" style="font-size: 20px;"></i> Hapus </button> -->
                    </div>
                  </div>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
             <div class="modal fade modal-danger " id="myModalstok" role="dialog" >
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <center><h3 class="modal-title"><i class="glyphicon glyphicon-exclamation-sign"style="font-size: 20px;"></i> PERINGATAN</h3></center> 
                  </div>
                  <div class="modal-body">
                    <h3><center>Apakah anda yakin ingin menghapus ? </h3><br/> </center>
                  </div>
                  <div class="modal-footer">
                  <a href="#" style="font-size: 20px;" id="modalDelete" type="button" class="btn btn-primary"><i class="glyphicon glyphicon-remove" style="font-size: 20px;"></i> YA </a>
                    <button type="button" style="font-size: 20px;" class="btn btn-danger pull-center" data-dismiss="modal">TIDAK</button>
                    
                  </div>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>                      
</form>


<!-- ./wrapper -->
<script src="<?php echo base_url() ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery 2.2.3 -->
<script type="text/javascript">
  $(function () {

    $('.hap').click(function(){
    //get cover id
    var id = $(this).data('id');
    //set href for cancel button
    $('#modalDelete').attr('href','<?php echo base_url(); ?>rbarang/hapus/'+id);
})


         $('#select_all').click(function(event) {
        if(this.checked) {
      // Iterate each checkbox
      $(':checkbox').each(function() {
          this.checked = true;
      });
      }
      else {
        $(':checkbox').each(function() {
              this.checked = false;
          });
      }
      });

    $("#example1").DataTable({
      "aoColumnDefs": [
           {

               "bSortable": false,
               "aTargets": ["tutup"]
           }
        ],
        "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "Semua"] // change per page values here
              ],
        "language": {
                "lengthMenu": "Tampilkan _MENU_ data",
                "paginate": {
                  "previous":"Prev",
                  "next": "Next",
                  "last": "Last",
                  "first": "First"
                }
              },


    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });


</script>