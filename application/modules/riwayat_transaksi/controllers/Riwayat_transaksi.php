<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Riwayat_transaksi extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_riwayat_transaksi');

		$this->load->helper('url');
		$ses = $this->session->userdata('sukacita');
		if(!$ses){
			redirect(base_url('login'));
		}
	}

	public function index(){
		$sess = $this->session->userdata('sukacita');
		$data['nama'] = $sess['nama'];
		$data['nama_role'] = $sess['nama_role'];
		$data['aktif'] = 'riwayat_transaksi';
		$data['nama_modul'] = "MANAJEMEN RIWAYAT TRANSAKSI";
		$data['title'] = "MANAJEMEN RIWAYAT TRANSAKSI";
		$data['gol'] = $this->M_riwayat_transaksi->get_riwayat()->result();
		
		$this->load->view('header',$data);
         $this->load->view('sidebar');
		$this->load->view('V_index_riwayat_transaksi',$data);
		$this->load->view('footer');

	}

	public function hapus($id){
		$this->M_riwayat_transaksi->hapus($id);
		redirect(base_url('riwayat_transaksi'));
	}

	public function  hapus_banyak() {
			$delcount = count($this->input->post('msg'));
			$data = $this->M_riwayat_transaksi->hapus_semua();
			if($data['total'] === $data['deleted']){
				$this->session->set_flashdata('alert','
					<div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4 style="font-weight: bold;"><i class="icon fa fa-check"></i> SUKSE MENGHAPUS DATA!</h4>
                Sukses Menghapus '.$data['total'].' dari '.$data['deleted'].' data yang dipilih!
              </div>
					');
			}else if ($data['total'] > $data['deleted']) {
				$this->session->set_flashdata('alert','
					<div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4 style="font-weight: bold;"><i class="icon fa fa-warning"></i> PERINGATAN, TERJADI KESALAHAN!</h4>
                Hanya dapat menghapus '.$data['total'].' dari '.$data['deleted'].' data yang dipilih karena adanya gangguan pada data yang tidak valid, silahkan, hapus satu persatu data tersebut.
              </div>
					');

			}else{
				$this->session->set_flashdata('alert','
					<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> GAGAL MENGHAPUS DATA!</h4>
                Gagal Menghapus data multiple dikarenakan terdapat masalah pada jaringan atau sistem silahkan ulangi lagi proses penginputan/hubungi pihak administrator!
              </div>
					');
			}
			redirect('riwayat_transaksi');
			
		}


	public function lihat($id){
		$sess = $this->session->userdata('sukacita');
		$data['nama'] = $sess['nama'];
		$data['nama_role'] = $sess['nama_role'];
		$data['aktif'] = 'riwayat_transaksi';
		$data['nama_modul'] = "DETAIL RIWAYAT TRANSAKSI";
		$data['title'] = "DETAIL RIWAYAT TRANSAKSI";
		$data['title'] = "MANAJEMEN RIWAYAT TRANSAKSI";
		$data['v1'] = $this->M_riwayat_transaksi->riwayat($id);
		$data['v2'] = $this->M_riwayat_transaksi->get_riwayat_transaksi($id);
		$this->load->view('header',$data);
         $this->load->view('sidebar');
		$this->load->view('V_lihat_riwayat_transaksi',$data);
		$this->load->view('footer');

		
	}


	function cetak($id){
	$this->load->model('M_riwayat_transaksi');
	$this->load->library('printers');
	$data2 = $this->M_riwayat_transaksi->riwayat($id);
	$data = $this->M_riwayat_transaksi->get_riwayat_transaksi($id);
	$this->printers->connect("POS-58");
	$this->printers->print_test_receipt($data,$data2);
	redirect(base_url('Riwayat_transaksi'));
}
	
	function cetak2($id){
	$this->load->model('M_riwayat_transaksi');
	$this->load->library('printers');
	$data2 = $this->M_riwayat_transaksi->riwayat($id);
	$data = $this->M_riwayat_transaksi->get_riwayat_transaksi($id);
	$this->printers->connect("POS-58");
	$this->printers->print_test_receipt($data,$data2);
	redirect(base_url('Riwayat_transaksi/lihat/'.$id));
}
	
}
