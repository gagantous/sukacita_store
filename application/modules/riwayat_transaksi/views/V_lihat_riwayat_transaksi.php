<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo strtoupper(@$nama_modul) ?>
      </h1>
      
    </section>

    <!-- Main content -->
    <section class="content">
    
       <div class="example-modal"> 
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="mod">
                 <div class="modal-header">
                  <a href="<?php echo(base_url('riwayat_transaksi')) ?>" class="btn btn-danger"><i class="fa fa-arrow-left"></i> KEMBALI</a>
                  <?php  foreach ($v1 as $v1) { ?>
                <h4 class="modal-title">NOTA : <?php echo $v1->no_nota  ?></h4>
                <div class="modal-title "></div>
              </div>
               <div class="box-body no-padding">
              <table class="table table-condensed">

                <tr>
                  <th style="width: 30px">No</th>
                  <th>Id barang</th>
                  <th>Barang</th>
                  <th>Qty</th>
                  <th>Harga</th>
                  <th>Sub</th>
                </tr>
                 <?php  
                 $count=0;
                 foreach ($v2 as $v2) { 
                    $count++;
                  ?>
                <tr>
                  <td><?php echo $count; ?></td>
                  <td><?php echo $v2->id_barang; ?></td>
                  <td><?php if($v2->id_barang == "0") {echo $v2->barang_m;} else{echo $v2->nama_barang;} ?></td>
                  <td><?php echo $v2->qty; ?></td>
                  <td>Rp <?php echo number_format(($v2->subtotal/$v2->qty),0,',','.'); ?></td>
                  <td>Rp <?php echo number_format($v2->subtotal,0,',','.'); ?></td>

                </tr>
              <?php   } ?>

                <tr>
                <td></td><td></td><td></td><td></td>
                <td>Total</td><td>Rp <?php echo number_format(($v1->bayar - $v1->kembali),0,',','.'); ?></td>
                </tr>
                <tr>
                <td></td><td></td><td></td><td></td>
                <td>Bayar</td><td>Rp <?php echo number_format($v1->bayar,0,',','.') ?></td>
                </tr>
                <tr>
                <td></td><td></td><td></td><td></td>
                <td>Kembali</td><td>Rp <?php echo number_format($v1->kembali,0,',','.') ?></td>
                </tr>
              </table>

             </div>
             </div>

                
              <div class="modal-footer">
               
               <button type="button" onclick="printdata(this.id)" id="<?php echo $v1->id ?>"class="btn btn-md btn-info"><i class="glyphicon glyphicon-print"></i></button>

              </div>
              <?php } ?>
            </div>
           
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        <!-- /.modal -->
      </div>
      <!-- /.error-page -->
    </section>
    <!-- /.content -->
  </div>

<script type="text/javascript">
     

      function printdata(clicked_id){location.href = "<?php echo base_url(); ?>riwayat_transaksi/cetak2/"+clicked_id;}
     
</script>