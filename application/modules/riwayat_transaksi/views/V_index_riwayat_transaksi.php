
      <!-- Main content -->
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        <?php echo strtoupper(@$nama_modul) ?>
      </h1>
    </section>
    <section class="content">
      <?php echo $this->session->flashdata('alert')?>
      <div class="row">
      <div class="col-md-12">
          <div class="box box-primary">
             <div class="box-header with-border"">
                 <a type="button" class="pull-left btn btn-md btn-danger" data-toggle="modal" data-target="#myModalstok1" role="button">HAPUS (Tertentu)</a>
             </div>
              <!-- /.box-header -->
            <div class="box-body">
             <form  action="<?php echo base_url() ?>riwayat_transaksi/hapus_banyak" method="POST" >
                <table id="example1"  class="table table-bordered table-striped table-hover">
                  <thead>
                  <tr>
                    <th class="tutup"><input type="checkbox" id="select_all" /></th>
                    <th>No</th>
                    <th>Nomor Nota</th>
                    <th>Pembayaran</th>
                    <th>Kembalian</th>
                   <!-- <th>Pegawai</th> -->
                    <th>Tanggal Transaksi</th>
                    <th>Lanjut</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php 
                  $no=1;
                  foreach ($gol as $key){ 
                    
                      ?>
                  <tr>
                    <td><input type="checkbox" name="msg[]" value="<?php echo $key->id; ?>"/></td>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $key->no_nota ?></td>
                    <td>Rp. <?php echo number_format($key->bayar,0,',','.')?></td>
                    <td>Rp. <?php echo number_format(($key->kembali),0,',','.')?></td>
                    <!-- <td><?php echo $key->user?></td> -->
                    <td><?php $month_num = date('d F Y', strtotime($key->tgl_transaksi));
                        echo $month_num; ?></td>
  				        <td >
                  <div class="btn-group">
                  <button type="button" onclick="lihatdata(this.id)" id="<?php echo $key->id ?>"class="btn btn-md btn-info "><i class="glyphicon glyphicon-eye-open"></i></button>
                  <button type="button" onclick="printdata(this.id)" id="<?php echo $key->id ?>"class="btn btn-md btn-info"><i class="glyphicon glyphicon-print"></i></button>
                   <?php if($nama_role == 'admin' || $nama_role == 'super') { ?>
                   <a href="#myModalstok" type="button" data-id="<?php echo $key->id ?>"  data-toggle="modal" class="hap btn btn-md btn-danger"><i class="glyphicon glyphicon-trash"></i></a>
                   <?php } ?>
                   </div>
                   </td>

                  </tr>
                  <?php } ?>
                  
                  </tbody>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
       <div class="modal fade modal-danger " id="myModalstok1" role="dialog" >
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <center><h3 class="modal-title"><i class="glyphicon glyphicon-exclamation-sign"style="font-size: 20px;"></i> PERINGATAN</h3></center> 
                  </div>
                  <div class="modal-body">
                    <h3><center>Apakah anda yakin ingin<br/>menghapus data-data yang sudah anda pilih  ? </h3><br/> </center>
                  </div>
                  <div class="modal-footer">
                    <button type="button" style="font-size: 20px;" class="btn btn-danger pull-left" data-dismiss="modal">BATAL</button>
                     <button type="submit" style="font-size: 20px;" name="submit" class="btn btn-primary">HAPUS</button>
                    <!-- <button type="button" style="font-size: 20px;" onclick="hapusstoksemua()" class="btn btn-outline"><i class="glyphicon glyphicon-remove" style="font-size: 20px;"></i> Hapus </button> -->
                  </div>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
             <div class="modal fade modal-danger " id="myModalstok" role="dialog" >
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <center><h3 class="modal-title"><i class="glyphicon glyphicon-exclamation-sign"style="font-size: 20px;"></i> PERINGATAN</h3></center> 
                  </div>
                  <div class="modal-body">
                    <h3><center>Apakah anda yakin ingin menghapus ? </h3><br/> </center>
                  </div>
                  <div class="modal-footer">
                    <button type="button" style="font-size: 20px;" class="btn btn-outline pull-left" data-dismiss="modal">Kembali</button>
                    <a href="#" style="font-size: 20px;" id="modalDelete" type="button" class="btn btn-outline"><i class="glyphicon glyphicon-remove" style="font-size: 20px;"></i> Hapus </a>
                  </div>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
                      
      </form>

   
      
  
           <script src="<?php echo base_url() ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>

  <script type="text/javascript">
      function lihatdata(clicked_id){location.href = "<?php echo base_url(); ?>riwayat_transaksi/lihat/"+clicked_id;}

      function printdata(clicked_id){location.href = "<?php echo base_url(); ?>riwayat_transaksi/cetak/"+clicked_id;}
     
      
             
  $(function () {

    $('.hap').click(function(){
    //get cover id
    var id = $(this).data('id');
    //set href for cancel button
    $('#modalDelete').attr('href','<?php echo base_url(); ?>riwayat_transaksi/hapus/'+id);
})


         $('#select_all').click(function(event) {
        if(this.checked) {
      // Iterate each checkbox
      $(':checkbox').each(function() {
          this.checked = true;
      });
      }
      else {
        $(':checkbox').each(function() {
              this.checked = false;
          });
      }
      });

   $("#example1").DataTable({
      "aoColumnDefs": [
           {

               "bSortable": false,
               "aTargets": ["tutup"]
           }
        ],
        "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "Semua"] // change per page values here
              ],
        "language": {
                "lengthMenu": "Tampilkan _MENU_ data",
                "paginate": {
                  "previous":"Prev",
                  "next": "Next",
                  "last": "Last",
                  "first": "First"
                }
              },


    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });


</script>