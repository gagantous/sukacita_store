<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
       <li class="<?php  if(@$aktif == 'dashboard') echo "active" ?>">
          <a href="<?php echo base_url(); ?>dashboard">
            <i class="fa fa-dashboard"></i> <span>DASHBOARD</span>
          </a>
        </li>
        <?php if(@$nama_role == 'admin' || @$nama_role == 'super') { ?>
        <li class="<?php  if(@$aktif == 'user') echo "active" ?>">
          <a href="<?php echo base_url(); ?>user">
            <i class="fa fa-dashboard"></i> <span>MANAJEMEN USER</span>
            
          </a>
        </li>
       <?php } ?>
        <li class="<?php  if(@$aktif == 'stok') echo "active" ?>" >
          <a href="<?php echo base_url(); ?>stok">
            <i class="fa fa-table"></i> <span>MANAJEMEN STOK</span>
          </a>
        </li>
        <li class="treeview <?php  if(@$aktif == 'transaksi') echo "active" ?> ">
          <a href="#">
            <i class="fa fa-table"></i> <span>TRANSAKSI</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu <?php  if(@$aktif == 'transaksi') echo "menu-open" ?> ">
            <li <?php  if(@$aktif == 'user') echo "active" ?> ><a href="<?php echo base_url(); ?>transaksi"><i class="fa fa-circle-o"></i>KASIR</a></li>
            <!-- <li><a href="<?php echo base_url(); ?>nota"><i class="fa fa-circle-o"></i>Nota</a></li> -->
          </ul>
        </li>

        <li class="header">Menu Lainnya</li>
        <li class="<?php  if(@$aktif == 'riwayat_transaksi') echo "active" ?>" ><a href="<?php echo base_url();?>riwayat_transaksi"><i class="fa fa-circle-o text-red"></i><span>RIWAYAT TRANSAKSI</span></a></li>
        <?php if($nama_role == 'admin' || $nama_role == 'super') { ?> 
        <li class="<?php  if(@$aktif == 'riwayat_barang') echo "active" ?>" ><a href="<?php echo base_url();?>rbarang"><i class="fa fa-circle-o text-red"></i><span>RIWAYAT BARANG</span></a></li><?php } ?>
       <!--  <?php if($nama_role == 'admin' || $nama_role == 'super') { ?> 
          <li class="header">Pengaturan lebih lanjut</li>
          <li class="treeview <?php  if(@$aktif == 'import' || @$aktif == 'export' || @$aktif == 'pengaturan_printer') echo "active" ?> ">
          <a href="#">
            <i class="fa fa-table"></i> <span>PENGATURAN</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu <?php  if(@$aktif == 'import' || @$aktif == 'export' || @$aktif == 'pengaturan_printer') echo "menu-open" ?>">
            <li class="<?php  if(@$aktif == 'import') echo "active" ?>" ><a href="<?php echo base_url(); ?>pengaturan/import"><i class="fa fa-circle-o"></i>IMPORT DATABASE</a></li>
            <li class="<?php  if(@$aktif == 'export') echo "active" ?>"><a href="<?php echo base_url(); ?>pengaturan/export"><i class="fa fa-circle-o"></i>EXPORT DATABASE</a></li>
            <li class="<?php  if(@$aktif == 'pengaturan') echo "active" ?>"><a href="<?php echo base_url(); ?>pengaturan/printer"><i class="fa fa-circle-o"></i>PENGATURAN</a></li>
             <li><a href="<?php echo base_url(); ?>nota"><i class="fa fa-circle-o"></i>Nota</a></li> 
          </ul>
        </li>
        <?php } ?> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>