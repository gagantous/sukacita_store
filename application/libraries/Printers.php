<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// IMPORTANT - Replace the following line with your path to the escpos-php autoload script
require_once (APPPATH.'../escpos/autoload.php');
use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;


function columnify($leftCol,$midCol,$mid2Col,$rightCol,$leftWidth,$midWidth,$mid2Width,$rightWidth, $space = 50)
{
    $leftWrapped = wordwrap($leftCol, $leftWidth, "\n", true);
    $midWrapped = wordwrap($midCol, $midWidth, "\n", true);
    $mid2Wrapped = wordwrap($mid2Col, $mid2Width, "\n", true);
    $rightWrapped = wordwrap($rightCol, $rightWidth, "\n", true);

    $leftLines = explode("\n", $leftWrapped);
    $midLines = explode("\n", $midWrapped);
    $mid2Lines = explode("\n", $mid2Wrapped );
    $rightLines = explode("\n", $rightWrapped);
    $allLines = array();
    for ($i = 0; $i < max(count($leftLines), count($midLines), count($mid2Lines),count($rightLines)); $i ++) {
        $leftPart = str_pad(isset($leftLines[$i]) ? $leftLines[$i] : "", $leftWidth, " ");
        $midPart = str_pad(isset($midLines[$i]) ? $midLines[$i] : "", $midWidth, " ");
        $mid2Part = str_pad(isset($mid2Lines[$i]) ? $mid2Lines[$i] : "", $mid2Width, " ");
        $rightPart = str_pad(isset($rightLines[$i]) ? $rightLines[$i] : "", $rightWidth, " ");

        $allLines[] = $leftPart.str_repeat(" ", $space).$midPart.str_repeat(" ", $space).$mid2Part.str_repeat(" ", $space).$rightPart;
    }
    return implode($allLines, "\n") . "\n";
}

function smart_wordwrap($string, $width = 18, $break = "\n") {
    // split on problem words over the line length
    $pattern = sprintf('/([^ ]{%d,})/', $width);
    $output = '';
    $words = preg_split($pattern, $string, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

    foreach ($words as $word) {
        if (false !== strpos($word, ' ')) {
            // normal behaviour, rebuild the string
            $output .= $word;
        } else {
            // work out how many characters would be on the current line
            $wrapped = explode($break, wordwrap($output, $width, $break));
            $count = $width - (strlen(end($wrapped)) % $width);

            // fill the current line and add a break
            $output .= substr($word, 0, $count) . $break;

            // wrap any remaining characters from the problem word
            $output .= wordwrap(substr($word, $count), $width, $break, true);
        }
    }

    // wrap the final output
    return wordwrap($output, $width, $break);
  }

class Printers {
  private $CI;
  private $connector;
  private $printer;
  // TODO: printer settings
  // Make this configurable by printer (32 or 48 probably)
  private $printer_width = 32;

  
  function __construct()
  {
    $this->CI =& get_instance(); // This allows you to call models or other CI objects with $this->CI->... 
  }

  function connect($ip_address)
  {
    $this->connector = new WindowsPrintConnector($ip_address);
    $this->printer = new Printer($this->connector);
  }

  private function check_connection()
  {
    if (!$this->connector OR !$this->printer OR !is_a($this->printer, 'Mike42\Escpos\Printer')) {
      throw new Exception("Tried to create receipt without being connected to a printer.");
    }
  }

  public function close_after_exception()
  {
    if (isset($this->printer) && is_a($this->printer, 'Mike42\Escpos\Printer')) {
      $this->printer->close();
    }
    $this->connector = null;
    $this->printer = null;
    $this->emc_printer = null;
  }
  // Calls printer->text and adds new line
  private function add_line($text = "", $should_wordwrap = true)
  {
    $text = $should_wordwrap ? wordwrap($text, $this->printer_width) : $text;
    $this->printer->text($text."\n");
  }
  
  public function print_test_receipt($v2, $v3)
  {

    
    $this->check_connection();
    $x = 0;
    foreach ($v3 as $v3 ) {
      $tgl = $v3->tgl_transaksi;
      $sub = number_format(($v3->bayar-$v3->kembali),0,',','.');
      $bayar = number_format(($v3->bayar),0,',','.');
      $kembali = number_format(($v3->kembali),0,',','.');
      $nota = $v3->no_nota;
      $sub = new baru('Total',$sub);
      $bayar = new baru('Bayar',$bayar);
      $kembali = new baru('Kembali',$kembali);
    $x++;
    }
    //$items = array(new item($data));
    $this->printer->setJustification(Printer::JUSTIFY_CENTER);
    $this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $this->printer->setEmphasis(true);

    $this->add_line("TOKO SUKACITA");
    $this->add_line();
     $this->printer->selectPrintMode();
     $this->printer->selectPrintMode(Printer::MODE_FONT_B);
     
     $this->printer->selectPrintMode();
      $this->add_line("NO STRUK : ".$nota);
      $this->printer -> text($tgl . "\n");
      $this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
     $box = "".str_repeat("=", 16)."";

    $this->printer->text($box);
    $this->printer->selectPrintMode(Printer::MODE_FONT_B);
    $this->printer->text(new item('','qty','harga','jumlah')); // blank line
    foreach ($v2 as $v2) {
      $nama_barang = $v2->nama_barang;
      $id_barang = $v2->id_barang;
      $barang_m = $v2->barang_m;
      $qty = $v2->qty;
      $harga = number_format(($v2->subtotal/$v2->qty),0,',','.');
      $subtotal = number_format(($v2->subtotal),0,',','.');
      if($id_barang  == "0") {$nama_barang = $barang_m;} else{$nama_barang = $nama_barang ;} 
      $this->printer->setJustification();
    $this->printer->selectPrintMode(Printer::MODE_FONT_B);
    $this->printer->text(columnify($nama_barang,$qty,$harga,$subtotal,19,4,9,7, 1));
    
    $this->printer->selectPrintMode(); }
     $this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);

    $this->printer->setEmphasis(true);
    $this->printer->text($box);
    $this->printer->selectPrintMode();
    $this->printer->setJustification();
    $this->printer->setJustification(Printer::JUSTIFY_LEFT);
    $this->printer->text($sub);
    $this->printer->text($bayar);
     $this->printer->text($kembali);
     $this->printer->feed(1);
     $this->printer->setJustification(Printer::JUSTIFY_CENTER);
     $this->printer->text("BARANG YANG SUDAH DIBELI");
     $this->add_line();
     $this->printer->text("TIDAK DAPAT DITUKAR/DIKEMBALIKAN");
      $this->printer -> feed(2);
     $this->printer->text("Terima kasih sudah berbelanja!");
     $this->printer -> setJustification(Printer::JUSTIFY_CENTER);
    $this->printer -> feed(2);
    
     $this->printer -> feed(1);
    $this->printer->cut(Printer::CUT_PARTIAL);
    $this->printer->close();
  }
}

class baru
{
    private $name1;
    private $price1;
    private $dollarSign1;

    public function __construct($name1 = '', $price1 = '', $dollarSign1 = false)
    {
        $this -> name1 = $name1;
        $this -> price1 = $price1;
        $this -> dollarSign1 = $dollarSign1;
    }

    public function __toString()
    {
        $rightCols1 = 10;
        $leftCols1 = 20;
        if ($this -> dollarSign1) {
            $leftCols1 = $leftCols1 / 2 - $rightCols1 / 2;
        }
        $left1 = str_pad($this -> name1, $leftCols1) ;

        $sign1 = ($this -> dollarSign1);
        $right1 = str_pad($sign1 . $this -> price1, $rightCols1, ' ', STR_PAD_LEFT);
        return "$left1$right1\n";
    }
}

class item
{
    private $namesign;
    private $name;
    private $price;
    private $price2;
    private $dollarSign;
    private $dollarSign2;
    private $qtySign;
    private $qty;

    public function __construct($name = '', $price = '', $qty = '', $price2='' ,$namesign = false , $qtySign = false, $dollarSign2 = false, $dollarSign = false)
    {
        $this-> namesign = $namesign;
        $this -> name = $name;
        $this -> qty = $qty;
        $this -> price = $price;
        $this -> price2 = $price2;
        $this -> qtySign = $qtySign;
        $this -> dollarSign = $dollarSign;
        $this -> dollarSign2 =$dollarSign2;
    }

    public function __toString()
    {   
        $leftCols = 18;
        $rightCols = 2;
        $midCols = 9;
        $midCols2 = 10;
        
        if($this -> qtySign){
            $midCols = $midCols / 4 -  $rightCols/4  ;
        }
        if($this -> dollarSign2){
            $midCols2 = $midCols2 /8 - $midCols/8 ;
        }        
        if ($this -> dollarSign) {
            $leftCols = $leftCols / 6 - $midCols / 6;
        }
        $sign4 = ($this -> namesign ? 'nama' : '');
        $sign2 = ($this -> qtySign ? 'qty' : '');
        $sign3 = ($this -> dollarSign2 ? 'harga' : '');
        $sign = ($this -> dollarSign ? 'sub' : '');
        $mid = str_pad($sign2 . $this -> qty, $midCols,' ', STR_PAD_LEFT);
        $mid2 = str_pad($sign3 . $this -> price2, $midCols2,' ', STR_PAD_LEFT);
        $right = str_pad($sign . $this -> price, $rightCols,' ', STR_PAD_LEFT);
        $left = str_pad($sign4 . $this -> name, $leftCols,' ',STR_PAD_RIGHT);
        return "$left$right$mid$mid2\n";
    }
}